# Remove CustomName
data modify entity @s CustomName set value ""

# Leave Team and remove general Tag
team leave @s
tag @s remove en_tag_glowing_team

# Remove specific Tag
tag @s remove en_tag_glowing_team_aqua
tag @s remove en_tag_glowing_team_black
tag @s remove en_tag_glowing_team_blue
tag @s remove en_tag_glowing_team_dark_aqua
tag @s remove en_tag_glowing_team_dark_blue
tag @s remove en_tag_glowing_team_dark_gray
tag @s remove en_tag_glowing_team_dark_green
tag @s remove en_tag_glowing_team_dark_purple
tag @s remove en_tag_glowing_team_dark_red
tag @s remove en_tag_glowing_team_gold
tag @s remove en_tag_glowing_team_gray
tag @s remove en_tag_glowing_team_green
tag @s remove en_tag_glowing_team_light_purple
tag @s remove en_tag_glowing_team_red
tag @s remove en_tag_glowing_team_white
tag @s remove en_tag_glowing_team_yellow

