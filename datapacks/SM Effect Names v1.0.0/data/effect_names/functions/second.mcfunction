# EffectNames Info Trigger
execute as @a[scores={EffectNames=1..}] run function effect_names:info
scoreboard players enable @a EffectNames


# Despawn / Prevent Despawn
execute as @e[name="No Despawn",type=!player] run function effect_names:effects/no_despawn
execute as @e[name=Despawn,type=!player] run function effect_names:effects/despawn


# Mute / Unmute
execute as @e[name=Mute,type=!player,nbt=!{Silent:1b}] run function effect_names:effects/mute
execute as @e[name=Unmute,type=!player,nbt={Silent:1b}] run function effect_names:effects/unmute

# Ghost
execute as @e[name=Ghost,type=!player] run function effect_names:effects/ghost

# Baby / Adult
execute as @e[name=Baby,type=!player,tag=!en_tag_is_baby] run function effect_names:tag_baby
execute as @e[tag=en_tag_is_baby,nbt=!{Age:-25000}] run function effect_names:effects/baby
execute as @e[name=Adult,type=!player] run function effect_names:effects/adult


# Lefthanded / Righthanded
execute as @e[name=Lefthanded,type=!player,nbt=!{LeftHanded:1b}] run function effect_names:effects/lefthanded
execute as @e[name=Righthanded,type=!player,nbt=!{LeftHanded:0b}] run function effect_names:effects/righthanded

# Zombie Horse / Skeleton Horse
execute as @e[name=Zombie,type=skeleton_horse] run function effect_names:effects/zombie
execute as @e[name=Skeleton,type=zombie_horse] run function effect_names:effects/skeleton

# Team Colors
execute as @e[name="Glowing Aqua",type=!player,tag=!en_tag_glowing_team_aqua] run function effect_names:teams/aqua
execute as @e[name="Glowing Black",type=!player,tag=!en_tag_glowing_team_black] run function effect_names:teams/black
execute as @e[name="Glowing Blue",type=!player,tag=!en_tag_glowing_team_blue] run function effect_names:teams/blue
execute as @e[name="Glowing Dark Aqua",type=!player,tag=!en_tag_glowing_team_dark_aqua] run function effect_names:teams/dark_aqua
execute as @e[name="Glowing Dark Blue",type=!player,tag=!en_tag_glowing_team_dark_blue] run function effect_names:teams/dark_blue
execute as @e[name="Glowing Dark Gray",type=!player,tag=!en_tag_glowing_team_dark_gray] run function effect_names:teams/dark_gray
execute as @e[name="Glowing Dark Green",type=!player,tag=!en_tag_glowing_team_dark_green] run function effect_names:teams/dark_green
execute as @e[name="Glowing Dark Purple",type=!player,tag=!en_tag_glowing_team_dark_purple] run function effect_names:teams/dark_purple
execute as @e[name="Glowing Dark Red",type=!player,tag=!en_tag_glowing_team_dark_red] run function effect_names:teams/dark_red
execute as @e[name="Glowing Gold",type=!player,tag=!en_tag_glowing_team_gold] run function effect_names:teams/gold
execute as @e[name="Glowing Gray",type=!player,tag=!en_tag_glowing_team_gray] run function effect_names:teams/gray
execute as @e[name="Glowing Green",type=!player,tag=!en_tag_glowing_team_green] run function effect_names:teams/green
execute as @e[name="Glowing Light Purple",type=!player,tag=!en_tag_glowing_team_light_purple] run function effect_names:teams/light_purple
execute as @e[name="Glowing Red",type=!player,tag=!en_tag_glowing_team_red] run function effect_names:teams/red
execute as @e[name="Glowing White",type=!player,tag=!en_tag_glowing_team_white] run function effect_names:teams/white
execute as @e[name="Glowing Yellow",type=!player,tag=!en_tag_glowing_team_yellow] run function effect_names:teams/yellow
execute as @e[name="Remove Glowing",type=!player,tag=en_tag_glowing_team] run function effect_names:teams/remove
execute as @e[type=!player,tag=en_tag_glowing_team] run effect give @s minecraft:glowing 2 0 true

