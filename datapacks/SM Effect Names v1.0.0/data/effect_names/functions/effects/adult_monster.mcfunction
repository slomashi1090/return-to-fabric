
setblock ~ 0 ~ minecraft:jukebox{RecordItem:{id:"minecraft:stone",Count:1b,tag:{}}} replace
data modify block ~ 0 ~ RecordItem.tag merge from entity @s {}
data remove block ~ 0 ~ RecordItem.tag.IsBaby

execute as @s[type=minecraft:drowned] run summon minecraft:drowned ~ ~ ~ {Tags:[en_tag_adult]}
execute as @s[type=minecraft:husk] run summon minecraft:husk ~ ~ ~ {Tags:[en_tag_adult]}
execute as @s[type=minecraft:zombie] run summon minecraft:zombie ~ ~ ~ {Tags:[en_tag_adult]}
execute as @s[type=minecraft:zombie_villager] run summon minecraft:zombie_villager ~ ~ ~ {Tags:[en_tag_adult]}
execute as @s[type=minecraft:zombie_pigman] run summon minecraft:zombie_pigman ~ ~ ~ {Tags:[en_tag_adult]}

data modify entity @e[tag=en_tag_adult,limit=1,distance=..1] {} merge from block ~ 0 ~ RecordItem.tag
tag @e[tag=en_tag_adult,limit=1,distance=..1] remove en_tag_adult

setblock ~ 0 ~ minecraft:bedrock replace
data merge entity @s {DeathTime:19s,Health:0.0f}
