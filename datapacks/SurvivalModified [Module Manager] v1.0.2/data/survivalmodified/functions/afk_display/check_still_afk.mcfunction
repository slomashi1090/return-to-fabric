# Get current position
execute store result score @s sm_afk_x1 run data get entity @s Pos[0] 100
execute store result score @s sm_afk_y1 run data get entity @s Pos[1] 100
execute store result score @s sm_afk_z1 run data get entity @s Pos[2] 100
execute store result score @s sm_afk_hori1 run data get entity @s Rotation[0] 100
execute store result score @s sm_afk_verti1 run data get entity @s Rotation[1] 100

# Compare with position saved last second, if not same, set score to not afk
execute unless score @s sm_afk_x1 = @s sm_afk_x2 run scoreboard players set @s sm_player_is_afk 0
execute unless score @s sm_afk_y1 = @s sm_afk_y2 run scoreboard players set @s sm_player_is_afk 0
execute unless score @s sm_afk_z1 = @s sm_afk_z2 run scoreboard players set @s sm_player_is_afk 0
execute unless score @s sm_afk_hori1 = @s sm_afk_hori2 run scoreboard players set @s sm_player_is_afk 0
execute unless score @s sm_afk_verti1 = @s sm_afk_verti2 run scoreboard players set @s sm_player_is_afk 0

# If AFK display is enabled, leave AFK team
team leave @s[scores={sm_player_is_afk=0},team=sm_team_afk]


# Save current position to be compared next second
scoreboard players operation @s sm_afk_x2 = @s sm_afk_x1
scoreboard players operation @s sm_afk_y2 = @s sm_afk_y1
scoreboard players operation @s sm_afk_z2 = @s sm_afk_z1
scoreboard players operation @s sm_afk_hori2 = @s sm_afk_hori1
scoreboard players operation @s sm_afk_verti2 = @s sm_afk_verti1

#Set afk seconds to 0 so the afk timer doesnt go up for afk players
scoreboard players set @s sm_afk_seconds 0 
