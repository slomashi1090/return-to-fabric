# If called, set Setup to "complete"
scoreboard players set #sm_base_afk_prefix_color SurvivalModified 6
team modify sm_team_afk prefix {"text":" [AFK] ","color":"dark_gray"}

# Message
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Team Prefix: ","color":"green"},{"text":"[AFK]","color":"dark_gray"},{"text":".","color":"green"}]
