# If called, set Setup to "complete"
scoreboard players set #sm_base_afk_team_prefix SurvivalModified 2

team modify sm_team_afk prefix ""

# Message 
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Players will not have a prefix.","color":"red"}]
