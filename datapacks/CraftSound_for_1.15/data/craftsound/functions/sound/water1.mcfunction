playsound minecraft:item.bucket.fill player @a[distance=..12] ~ ~ ~ 1 1.1

scoreboard players reset @s[scores={c.cod_bucket=1..}] c.cod_bucket
scoreboard players reset @s[scores={c.dragon_breath=1..}] c.dragon_breath
scoreboard players reset @s[scores={c.xp_bottle=1..}] c.xp_bottle
scoreboard players reset @s[scores={c.lava_bucket=1..}] c.lava_bucket
scoreboard players reset @s[scores={c.linger_potion=1..}] c.linger_potion
scoreboard players reset @s[scores={c.milk=1..}] c.milk
scoreboard players reset @s[scores={c.potion=1..}] c.potion
scoreboard players reset @s[scores={c.pufferfish_buc=1..}] c.pufferfish_buc
scoreboard players reset @s[scores={c.salmon_bucket=1..}] c.salmon_bucket
scoreboard players reset @s[scores={c.splash_potion=1..}] c.splash_potion
scoreboard players reset @s[scores={c.trop_fish_buck=1..}] c.trop_fish_buck
scoreboard players reset @s[scores={c.water_bucket=1..}] c.water_bucket


