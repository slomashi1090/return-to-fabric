playsound minecraft:item.book.page_turn player @a[distance=..12] ~ ~ ~ 1 1

scoreboard players reset @s[scores={c.book=1..}] c.book
scoreboard players reset @s[scores={c.enchanted_book=1..}] c.enchanted_book
scoreboard players reset @s[scores={c.item_frame=1..}] c.item_frame
scoreboard players reset @s[scores={c.map=1..}] c.map
scoreboard players reset @s[scores={c.painting=1..}] c.painting
scoreboard players reset @s[scores={c.paper=1..}] c.paper
scoreboard players reset @s[scores={c.writable_book=1..}] c.writable_book
scoreboard players reset @s[scores={c.written_book=1..}] c.written_book
