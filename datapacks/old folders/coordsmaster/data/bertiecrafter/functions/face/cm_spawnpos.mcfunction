#Current As/At: Player who requested facing some coordinates

#First, remove own coords of requested coords. Then divide the new (relative) coords by just enough to be spawnable
#Finally add own coords back to the relative coords to create absolute coords.
#Summon and set pos of armor_stand to the calculated absolute corods.

#Make requested coords relative (to eyes)
execute store result score @s bccm_faceOX run data get entity @s Pos[0]
execute store result score @s bccm_faceOY run data get entity @s Pos[1]
execute store result score @s bccm_faceOZ run data get entity @s Pos[2]
scoreboard players operation @s bccm_faceX -= @s bccm_faceOX
scoreboard players operation @s bccm_faceY -= @s bccm_faceOY
scoreboard players operation @s bccm_faceZ -= @s bccm_faceOZ


#Get absolute relative position
scoreboard players operation @s bccm_faceAX = @s bccm_faceX
scoreboard players operation @s bccm_faceAY = @s bccm_faceY
scoreboard players operation @s bccm_faceAZ = @s bccm_faceZ
execute if score @s bccm_faceAX matches ..0 run scoreboard players operation @s bccm_faceAX *= #negative_one bccm_faceD
execute if score @s bccm_faceAY matches ..0 run scoreboard players operation @s bccm_faceAY *= #negative_one bccm_faceD
execute if score @s bccm_faceAZ matches ..0 run scoreboard players operation @s bccm_faceAZ *= #negative_one bccm_faceD
title @s actionbar {"text":"Distance: ","color":"white","extra":[{"text":"X ","color":"gold"},{"score":{"name":"*","objective":"bccm_faceAX"},"color":"yellow"},{"text":" | "},{"text":"Y ","color":"gold"},{"score":{"name":"*","objective":"bccm_faceAY"},"color":"yellow"},{"text":" | "},{"text":"Z ","color":"gold"},{"score":{"name":"*","objective":"bccm_faceAZ"},"color":"yellow"}]}

#Get closest position that is able to indicate direction
scoreboard players operation @s bccm_faceD = @s bccm_faceAY
scoreboard players operation @s bccm_faceD > @s bccm_faceAX
scoreboard players operation @s bccm_faceD > @s bccm_faceAZ
scoreboard players operation @s bccm_faceD /= #distance_cap bccm_faceD
scoreboard players operation @s bccm_faceD += #one bccm_faceD
scoreboard players operation @s bccm_faceX /= @s bccm_faceD
scoreboard players operation @s bccm_faceY /= @s bccm_faceD
scoreboard players operation @s bccm_faceZ /= @s bccm_faceD

#Turn relative coords back into absolute coords. However, now they're as close as they can get.
scoreboard players operation @s bccm_faceX += @s bccm_faceOX
scoreboard players operation @s bccm_faceY += @s bccm_faceOY
scoreboard players operation @s bccm_faceZ += @s bccm_faceOZ

#Summon armor_stand, face it and kill it
summon armor_stand ~ ~ ~ {Invulnerable:1b,NoGravity:1b,Tags:["bccm_facing_marker"],Invisible:1b}
execute store result entity @e[tag=bccm_facing_marker,limit=1,sort=nearest] Pos[0] double 1 run scoreboard players get @s bccm_faceX
execute store result entity @e[tag=bccm_facing_marker,limit=1,sort=nearest] Pos[1] double 1 run scoreboard players get @s bccm_faceY
execute store result entity @e[tag=bccm_facing_marker,limit=1,sort=nearest] Pos[2] double 1 run scoreboard players get @s bccm_faceZ
execute as @e[tag=bccm_facing_marker,limit=1,sort=nearest] at @s run tp @s ~0.5 ~0.5 ~0.5
execute anchored eyes facing entity @e[tag=bccm_facing_marker,limit=1,sort=nearest] feet run tp @s ~ ~ ~ ~ ~
kill @e[tag=bccm_facing_marker]
tag @s remove bccm_face