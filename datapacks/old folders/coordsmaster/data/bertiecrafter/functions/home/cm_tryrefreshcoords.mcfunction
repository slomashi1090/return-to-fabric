#Current As/At: Sleeping player
execute store result score #temp bccm_homeX run data get entity @s Pos[0]
execute store result score #temp bccm_homeY run data get entity @s Pos[1]
execute store result score #temp bccm_homeZ run data get entity @s Pos[2]
execute if score @s bccm_homeX = #temp bccm_homeX if score @s bccm_homeY = #temp bccm_homeY if score @s bccm_homeZ = #temp bccm_homeZ run tag @s add bccm_homeIsSame
execute as @s[tag=!bccm_homeIsSame] run function bertiecrafter:home/cm_refreshcoords
tag @s remove bccm_homeIsSame
scoreboard players set @s bccm_homeSleep 0
scoreboard players enable @s bccm_homeU