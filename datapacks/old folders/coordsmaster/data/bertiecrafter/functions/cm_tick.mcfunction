#Current As/At: Server
execute as @a[tag=!bccm_initiated] run function bertiecrafter:cm_initplayer
execute as @a[nbt={SelectedItem:{id:"minecraft:compass"}}] run function bertiecrafter:cm_showmenu

#Death coords
execute as @a[scores={bccm_death=1..}] at @s run function bertiecrafter:death/cm_refreshcoords
execute as @a[scores={bccm_deathU=1},nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:death/cm_unlockcoords
execute as @a[scores={bccm_deathU=1}] at @s run function bertiecrafter:death/cm_resetrequest
execute as @a[scores={bccm_deathF=1}] at @s run function bertiecrafter:death/cm_preparefacing

#End In End
execute as @a[scores={bccm_endDF=1}] at @s run function bertiecrafter:end_in_end/cm_preparefacing
execute as @a[scores={bccm_endDS=1},tag=!bccm_setEndInEnd,nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:end_in_end/cm_preparesetcoords
scoreboard players set @a[scores={bccm_endDS=1}] bccm_endDS 0
execute as @a[tag=bccm_setEndInEnd] at @s if block ~ ~-1 ~ minecraft:end_portal run function bertiecrafter:end_in_end/cm_setcoords

#End In Overworld
execute as @a[scores={bccm_endOF=1}] at @s run function bertiecrafter:end_in_overworld/cm_preparefacing
execute as @a[scores={bccm_endOS=1},tag=!bccm_setEndInOverworld,nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:end_in_overworld/cm_preparesetcoords
scoreboard players set @a[scores={bccm_endOS=1}] bccm_endOS 0
execute as @a[tag=bccm_setEndInOverworld] at @s if block ~ ~-1 ~ minecraft:end_portal run function bertiecrafter:end_in_overworld/cm_setcoords

#Home
execute as @a[scores={bccm_homeSleep=1..}] at @s run function bertiecrafter:home/cm_tryrefreshcoords
execute as @a[scores={bccm_homeU=1},nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:home/cm_unlockcoords
execute as @a[scores={bccm_homeU=1}] at @s run function bertiecrafter:home/cm_resetrequest
execute as @a[scores={bccm_homeF=1}] at @s run function bertiecrafter:home/cm_preparefacing

#Location 1
execute as @a[scores={bccm_locF=1}] at @s run function bertiecrafter:location1/cm_preparefacing
execute as @a[scores={bccm_locS=1},nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:location1/cm_setcoords
scoreboard players set @a[scores={bccm_locS=1}] bccm_locS 0

#Location 2
execute as @a[scores={bccm_locF=2}] at @s run function bertiecrafter:location2/cm_preparefacing
execute as @a[scores={bccm_locS=2},nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:location2/cm_setcoords
scoreboard players set @a[scores={bccm_locS=2}] bccm_locS 0

#Location 3
execute as @a[scores={bccm_locF=3}] at @s run function bertiecrafter:location3/cm_preparefacing
execute as @a[scores={bccm_locS=3},nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:location3/cm_setcoords
scoreboard players set @a[scores={bccm_locS=3}] bccm_locS 0

#Location 4
execute as @a[scores={bccm_locF=4}] at @s run function bertiecrafter:location4/cm_preparefacing
execute as @a[scores={bccm_locS=4},nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:location4/cm_setcoords
scoreboard players set @a[scores={bccm_locS=4}] bccm_locS 0

#Location 5
execute as @a[scores={bccm_locF=5}] at @s run function bertiecrafter:location5/cm_preparefacing
execute as @a[scores={bccm_locS=5},nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:location5/cm_setcoords
scoreboard players set @a[scores={bccm_locS=5}] bccm_locS 0

#Input
execute as @a[scores={bccm_inputF=1}] at @s run function bertiecrafter:input/cm_preparefacing
execute as @a[scores={bccm_inputS=1},nbt={Inventory:[{id:"minecraft:compass"}]}] at @s run function bertiecrafter:input/cm_allowsetcoords
scoreboard players set @a[scores={bccm_inputS=1}] bccm_inputS 0

#Facing
execute as @a[tag=bccm_face] at @s run function bertiecrafter:face/cm_correctcoords

#TP
execute as @e[tag=bccm_gateway] at @s unless entity @a[distance=..1] run setblock ~ ~ ~ air
execute as @e[tag=bccm_gateway] at @s unless entity @a[distance=..1] run kill @s
execute as @a[tag=bccm_tpNeeded,scores={bccm_tpD=-1},nbt={Dimension:-1}] at @s run function bertiecrafter:tp/internal/cm_place_gate
execute as @a[tag=bccm_tpNeeded,scores={bccm_tpD=0},nbt={Dimension:0}] at @s run function bertiecrafter:tp/internal/cm_place_gate
execute as @a[tag=bccm_tpNeeded,scores={bccm_tpD=1},nbt={Dimension:1}] at @s run function bertiecrafter:tp/internal/cm_place_gate