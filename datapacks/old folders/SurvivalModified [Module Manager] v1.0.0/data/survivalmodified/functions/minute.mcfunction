# Playtime into Minutes
 # Transfer to Display
execute as @a run scoreboard players operation @s sm_playtime_disp = @s sm_playtime_tick
 # Divide by 1200
execute as @a run scoreboard players operation @s sm_playtime_disp /= #sm_playtime_minutes SurvivalModified

# Check AFK
execute as @a unless score @s sm_player_is_afk matches 1 at @s run function survivalmodified:afk_display/check_afk
