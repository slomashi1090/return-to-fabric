# Welcome
execute if score #sm_reload_module_count SurvivalModified matches 0 run tellraw @a [{"text":"[SurvivalModified] ","color":"light_purple"},{"text":"No SurvivalModified module detected.","color":"red"}]
execute if score #sm_reload_module_count SurvivalModified matches 1 run tellraw @a [{"text":"[SurvivalModified] ","color":"light_purple"},{"text":"[1] ","color":"gold"},{"text":"SurvivalModified module detected:","color":"green"}]
execute if score #sm_reload_module_count SurvivalModified matches 2.. run tellraw @a [{"text":"[SurvivalModified] ","color":"light_purple"},{"text":"[","color":"gold"},{"score":{"name":"#sm_reload_module_count","objective":"SurvivalModified"},"color":"gold"},{"text":"] ","color":"gold"},{"text":"SurvivalModified modules detected:","color":"green"}]

 # Template
 #execute if score #sm_????_installed SurvivalModified matches 1 run tellraw @a [{"text":"• ???????","color":"aqua"}]


# List modules
# Anti Grief
 execute if score #sm_anti_grief_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Anti Grief","color":"aqua"}]
 # Anti Enderman Grief
 execute if score #sm_anti_enderman_grief_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Anti Enderman Grief","color":"aqua"}]
 # Anti Ender Dragon Grief
 execute if score #sm_anti_enderdragon_grief_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Anti Ender Dragon Grief","color":"aqua"}]
 # Anti Wither Grief
 execute if score #sm_anti_wither_grief_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Anti Wither Grief","color":"aqua"}]
 # Blood Moon 
 execute if score #sm_blood_moon_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Blood Moon","color":"aqua"}]
 # Buffed Totem
 execute if score #sm_buffed_totem_of_undying_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Buffed Totem of Undying","color":"aqua"}]
 # Colorful Shulkers
 execute if score #sm_colorful_shulkers_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Colorful Shulkers","color":"aqua"}]
 # Compressed Cobblestone
 execute if score #sm_compressed_cobblestone_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Compressed Cobblestone","color":"aqua"}]
 # Dragon Demands Battle
 execute if score #sm_dragon_demands_battle_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Dragon Demands Battle","color":"aqua"}]
 # Effect Names
 execute if score #sm_effect_names_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Effect Names","color":"aqua"}]
 # Extra Hard Difficulty
 execute if score #sm_extra_hard_difficulty_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Extra Hard Difficulty","color":"aqua"}]
 # Health Boost Rewards
 execute if score #sm_health_boost_rewards_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Health Boost Rewards","color":"aqua"}]
 # Heavy Armor
 execute if score #sm_heavy_armor_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Heavy Armor","color":"aqua"}]
 # Lazy Chunk Cleanup
 execute if score #sm_lazy_chunk_cleanup_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Lazy Chunk Cleanup","color":"aqua"}]
 # Leather Armor Effects
 execute if score #sm_leather_armor_effects_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Leather Armor Effects","color":"aqua"}]
 # Prevent AFK Fishing
 execute if score #sm_prevent_afk_fishing_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Prevent AFK Fishing","color":"aqua"}]
 # Prevent Sleeping
 execute if score #sm_prevent_sleeping_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Prevent Sleeping","color":"aqua"}]
 # Progressive Tools Crafting
 execute if score #sm_progressive_tools_crafting_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Progressive Tools Crafting","color":"aqua"}]
 # Scoreboard Displayer
 execute if score #sm_scoreboard_displayer_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Scoreboard Displayer","color":"aqua"}]
 # Stronger Shulker Bullets
 execute if score #sm_stronger_shulker_bullets_installed SurvivalModified matches 1 run tellraw @a [{"text":"• Stronger Shulker Bullets","color":"aqua"}]


