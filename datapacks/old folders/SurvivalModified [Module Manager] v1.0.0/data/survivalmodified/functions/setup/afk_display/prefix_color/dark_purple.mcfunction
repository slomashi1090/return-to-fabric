# If called, set Setup to "complete"
scoreboard players set #sm_base_afk_prefix_color SurvivalModified 8
team modify sm_team_afk prefix {"text":" [AFK] ","color":"dark_purple"}

# Message
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Team Prefix: ","color":"green"},{"text":"[AFK]","color":"dark_purple"},{"text":".","color":"green"}]
