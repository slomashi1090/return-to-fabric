# Remove Scoreboards
scoreboard objectives remove sm_armor 
scoreboard objectives remove sm_dragonkill 
scoreboard objectives remove sm_witherkill 
scoreboard objectives remove sm_level
scoreboard objectives remove sm_crafts
scoreboard objectives remove sm_damage_dealt
scoreboard objectives remove sm_damage_taken
scoreboard objectives remove sm_deaths
scoreboard objectives remove sm_items_enchant
scoreboard objectives remove sm_mob_kills
scoreboard objectives remove sm_playtime_disp
scoreboard objectives remove sm_playtime_tick
scoreboard objectives remove sm_villager_trad
scoreboard objectives remove sm_stone_mined
scoreboard objectives remove sm_player_kills
scoreboard objectives remove sm_raid_wins
scoreboard objectives remove sm_health
scoreboard objectives remove sm_food
scoreboard objectives remove sm_afk_x1
scoreboard objectives remove sm_afk_x2
scoreboard objectives remove sm_afk_y1
scoreboard objectives remove sm_afk_y2
scoreboard objectives remove sm_afk_z1
scoreboard objectives remove sm_afk_z2
scoreboard objectives remove sm_player_is_afk
team remove sm_team_afk
scoreboard objectives remove sm_Help
scoreboard objectives remove sm_PosX
scoreboard objectives remove sm_PosY
scoreboard objectives remove sm_PosZ
team remove sm_team_aqua
team remove sm_team_black
team remove sm_team_blue
team remove sm_team_darkaqua
team remove sm_team_darkblue
team remove sm_team_darkgray
team remove sm_team_darkgrn
team remove sm_team_darkprpl
team remove sm_team_dark_red
team remove sm_team_gold
team remove sm_team_gray
team remove sm_team_green
team remove sm_team_lghtprpl
team remove sm_team_red
team remove sm_team_white
team remove sm_team_yellow

# Reset all SurvivalModified Scores
scoreboard players reset #sm_base_auto_setup_finished


# Disable manager
datapack disable "file/SurvivalModified [Module Manager] v1.0.0"

# Message
tellraw @s [{"text":"[SurvivalModified] ","color":"light_purple"},{"text":"Uninstall of [SurvivalModified Module Manager] successful.","color":"green"}]

tellraw @s [{"text":"[SurvivalModified] ","color":"light_purple"},{"text":"If you have finished or are planning on uninstalling all other SurvivalModified modules, click this message. If you only want to reinstall the module manager, do not click this message. Otherwise it will reset all modules.","color":"red","clickEvent":{"action":"run_command","value":"/scoreboard objectives remove SurvivalModified"}}]

