# Reset trigger
scoreboard players set @s sm_Help 0

# Welcome
tellraw @s [{"text":"[SurvivalModified Help] ","color":"light_purple"},{"text":"Hover over the module's name you would like to know more about. ","color":"aqua"},{"text":"By clicking ","color":"aqua"},{"text":"[Setup] ","color":"gold"},{"text":"you can reinstall or change the settings of the module.","color":"aqua"}]

 # Template
 #execute if score #sm_????_installed SurvivalModified matches 1 run tellraw @s [{"text":"????","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"","color":"aqua"}},"clickEvent":{"action":"run_command","value":""}}]
 # Setup
 # ["clickEvent":{"action":"run_command","value":"/trigger EffectNames"}}]


# Help
 # SurvivalModified Base Pack
 tellraw @s [{"text":"[SurvivalModified Module Manager] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"All SurvivalModified modules rely on this pack for its unified scoreboards, tick, second, and minute tracker and other features.","color":"aqua"}}},{"text":"[Setup]","color":"gold","clickEvent":{"action":"run_command","value":"/function survivalmodified:setup/manual_setup"}}]
 # Anti Grief
 execute if score #sm_anti_grief_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Anti Grief] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Prevents TNT, TNT Minecarts, Creepers, Ghasts and End Crystals from breaking blocks, mobs from picking up items and zombies from breaking doors; also manages AntiGrief Expansions.","color":"aqua"}}},{"text":"[Setup]","color":"gold","clickEvent":{"action":"run_command","value":"/function antigrief:setup/manual_setup"}}]
 # Anti Ender Dragon Grief
 execute if score #sm_anti_enderdragon_grief_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Anti Ender Dragon Grief]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Prevents the Ender Dragon from breaking blocks.","color":"aqua"}}}]
 # Anti Enderman Grief
 execute if score #sm_anti_enderman_grief_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Anti Enderman Grief]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Prevents Endermen from picking up blocks.","color":"aqua"}}}]
  # Anti Wither Grief
 execute if score #sm_anti_wither_grief_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Anti Wither Grief]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Prevents the Wither from breaking blocks. The shot Wither Skulls still break blocks.","color":"aqua"}}}]
 # Blood Moon (Setup)
 execute if score #sm_blood_moon_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Blood Moon]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Adds a new night event with custom mobs.","color":"aqua"}}},{"text":"[Setup]","color":"gold","clickEvent":{"action":"run_command","value":"/function blood_moon:setup/manual_setup"}}]
 # Buffed Totem
 execute if score #sm_buffed_totem_of_undying_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Buffed Totem of Undying]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Gives stronger Regeneration and Absorption, Speed, Fire Resistance and as negative effects Weakness and Nausea.","color":"aqua"}}}]
 # Colorful Shulkers
 execute if score #sm_colorful_shulkers_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Colorful Shulkers]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Colors Shulkers randomly in one of 17 colors.","color":"aqua"}}}]
 # Compressed Cobblestone
 execute if score #sm_compressed_cobblestone_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Compressed Cobblestone]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Adds craftable Cobblestone variants.","color":"aqua"}}}]
# Dragon Demands Battle
 execute if score #sm_dragon_demands_battle_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Dragon Demands Battle] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"The Ender Dragon prevents players from leaving the main island until it has been defeated.","color":"aqua"}}},{"text":"[Setup]","color":"gold","clickEvent":{"action":"run_command","value":"/function dragon_demands_battle:setup/manual_setup"}}]
 # Effect Names
 execute if score #sm_effect_names_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Effect Names]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Use /trigger EffectNames or click this to see more info.","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/trigger EffectNames"}}]
 # Extra Hard Difficulty
 execute if score #sm_extra_hard_difficulty_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Extra Hard]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"All mobs gain +50% HP and +50% damage. Additionally, cave mobs gain armor. Pets gain huge resistances. Bosses / Minibosses gain a few bigger stats.","color":"aqua"}}}]
 # Health Boost Rewards
 execute if score #sm_health_boost_rewards_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Health Boost Rewards]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Completing certain tasks rewards the player with bonus health.","color":"aqua"}}}]
 # Heavy Armor
 execute if score #sm_heavy_armor_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Heavy Armor]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Armor is harder to craft and slows players down the more armor points it gives. Slowness can be prevented by crafting a Light Feather.","color":"aqua"}}}]
 # Lazy Chunk Cleanup
 execute if score #sm_lazy_chunk_cleanup_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Lazy Chunk Cleanup] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Instead of relying on the Minecraft mechanism, mobs get killed when they are too far away from the player, depending on render distance, to prevent monsters unloading.","color":"aqua"}}},{"text":"[Setup]","color":"gold","clickEvent":{"action":"run_command","value":"/function lazy_chunk_cleanup:setup/manual_setup"}}]
 # Leather Armor Effects
 execute if score #sm_leather_armor_effects_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Leather Armor Effects]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Having a unified colored Leather Armor gives the wearer positive effects, depending on the armor color.","color":"aqua"}}}]
# Prevent AFK Fishing
 execute if score #sm_prevent_afk_fishing_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Prevent AFK Fishing]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Kills fishing bobbers near AFK players holding a fishing rod.","color":"aqua"}}}]
 # Prevent Sleeping
 execute if score #sm_prevent_sleeping_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Prevent Sleeping]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Prevents the player from skipping the night. Can still set their spawn and prevent phantoms from spawning by laying in bed for a moment.","color":"aqua"}}}]
 # Progressive Tools Crafting
 execute if score #sm_progressive_tools_crafting_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Progressive Tools Crafting]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Makes tools cost a few more ingredients.","color":"aqua"}}}]
 # Scoreboard Displayer
 execute if score #sm_scoreboard_displayer_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Scoreboard Displayer] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"By using /trigger ChangeScoreboard a player can change the displayed scoreboard in the list.","color":"aqua"}}},{"text":"[Setup]","color":"gold","clickEvent":{"action":"run_command","value":"/function scoreboard_displayer:setup/manual_setup"}}]
 # Stronger Shulker Bullets
 execute if score #sm_stronger_shulker_bullets_installed SurvivalModified matches 1 run tellraw @s [{"text":"[Stronger Shulker Bullets]","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Shulker bullets deal 12 damage instead of 4. This damage can be reduced using Protection enchantments and the resistance effect. Armor points do not reduce this damage.","color":"aqua"}}}]


