# (ax + c) % m

# If number is negative, turn positive
execute if score #sm_prng_temporary SurvivalModified matches ..0 run scoreboard players operation #sm_prng_temporary SurvivalModified *= #sm_negative_one SurvivalModified

# calculate a * x
scoreboard players operation #sm_prng_temporary SurvivalModified *= #sm_prng_a SurvivalModified

# calculate ax + c
scoreboard players operation #sm_prng_temporary SurvivalModified += #sm_prng_c SurvivalModified

# Modulo: 100
execute unless score #sm_prng_max_result SurvivalModified matches 1.. run function sm_prng:modulo_100

# Modulo: Custom Number
execute if score #sm_prng_max_result SurvivalModified matches 1.. run function sm_prng:modulo_custom

