execute unless score -session vs_mh_config matches 0 unless score -session vs_mh_config matches 2..3 as @a[scores={modularhud=100}] run scoreboard players add @s vs_mh_d_session 1
execute unless score -position vs_mh_config matches 0 unless score -position vs_mh_config matches 2..3 as @a[scores={modularhud=200}] run scoreboard players add @s vs_mh_d_position 1
execute unless score -hmobs vs_mh_config matches 0 unless score -hmobs vs_mh_config matches 2..3 as @a[scores={modularhud=300}] run scoreboard players add @s vs_mh_d_hmobs 1
execute unless score -pitch vs_mh_config matches 0 unless score -pitch vs_mh_config matches 2..3 as @a[scores={modularhud=400}] run scoreboard players add @s vs_mh_d_pitch 1
execute unless score -time vs_mh_config matches 0 unless score -time vs_mh_config matches 2..3 as @a[scores={modularhud=500}] run scoreboard players add @s vs_mh_d_time 1
execute unless score -direction vs_mh_config matches 0 unless score -direction vs_mh_config matches 2..3 as @a[scores={modularhud=600}] run scoreboard players add @s vs_mh_d_dir 1
execute unless score -compact vs_mh_config matches 0 unless score -compact vs_mh_config matches 2 as @a[scores={modularhud=700}] run scoreboard players add @s vs_mh_d_compact 1
execute unless score -displayhud vs_mh_config matches 0 unless score -displayhud vs_mh_config matches 2 as @a[scores={modularhud=800}] run scoreboard players add @s vs_mh_d_display 1

execute unless score -session vs_mh_config matches 0 unless score -session vs_mh_config matches 2..3 as @a[scores={modularhud=100}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute unless score -position vs_mh_config matches 0 unless score -position vs_mh_config matches 2..3 as @a[scores={modularhud=200}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute unless score -hmobs vs_mh_config matches 0 unless score -hmobs vs_mh_config matches 2..3 as @a[scores={modularhud=300}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute unless score -pitch vs_mh_config matches 0 unless score -pitch vs_mh_config matches 2..3 as @a[scores={modularhud=400}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute unless score -time vs_mh_config matches 0 unless score -time vs_mh_config matches 2..3 as @a[scores={modularhud=500}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute unless score -direction vs_mh_config matches 0 unless score -direction vs_mh_config matches 2..3 as @a[scores={modularhud=600}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute unless score -compact vs_mh_config matches 0 unless score -compact vs_mh_config matches 2 as @a[scores={modularhud=700}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute unless score -displayhud vs_mh_config matches 0 unless score -displayhud vs_mh_config matches 2 as @a[scores={modularhud=800}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1

execute as @a[scores={modularhud=1..,vs_mh_d_display=0}] run scoreboard players set @s vs_mh_d_display 1

execute as @a[scores={vs_mh_d_display=2..}] run title @s actionbar ""

execute as @a[scores={vs_mh_d_session=2..}] run scoreboard players set @s vs_mh_d_session 0
execute as @a[scores={vs_mh_d_position=2..}] run scoreboard players set @s vs_mh_d_position 0
execute as @a[scores={vs_mh_d_hmobs=2..}] run scoreboard players set @s vs_mh_d_hmobs 0
execute as @a[scores={vs_mh_d_pitch=2..}] run scoreboard players set @s vs_mh_d_pitch 0
execute as @a[scores={vs_mh_d_time=2..}] run scoreboard players set @s vs_mh_d_time 0
execute as @a[scores={vs_mh_d_dir=2..}] run scoreboard players set @s vs_mh_d_dir 0
execute as @a[scores={vs_mh_d_compact=2..}] run scoreboard players set @s vs_mh_d_compact 0
execute as @a[scores={vs_mh_d_display=2..}] run scoreboard players set @s vs_mh_d_display 0

execute as @a[scores={modularhud=1..,vs_mh_d_session=0,vs_mh_d_position=0,vs_mh_d_hmobs=0,vs_mh_d_pitch=0,vs_mh_d_time=0,vs_mh_d_dir=0}] run title @s actionbar ""

execute as @a[scores={modularhud=1..}] run scoreboard players set @s modularhud 0
execute as @a[scores={modularhud=..-1}] run scoreboard players set @s modularhud 0