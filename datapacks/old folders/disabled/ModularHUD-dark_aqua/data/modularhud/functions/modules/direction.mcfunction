execute as @a[scores={vs_mh_d_dir=1}] store result score @s vs_mh_direction run data get entity @s Rotation[0]
execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=-360..-1}] run scoreboard players operation @s vs_mh_direction += -360 vs_mh_constants

execute as @a[scores={vs_mh_d_dir=1,vs_mh_dir_n=1}] run scoreboard players set @s vs_mh_dir_n 0
execute as @a[scores={vs_mh_d_dir=1,vs_mh_dir_ne=1}] run scoreboard players set @s vs_mh_dir_ne 0
execute as @a[scores={vs_mh_d_dir=1,vs_mh_dir_e=1}] run scoreboard players set @s vs_mh_dir_e 0
execute as @a[scores={vs_mh_d_dir=1,vs_mh_dir_se=1}] run scoreboard players set @s vs_mh_dir_se 0
execute as @a[scores={vs_mh_d_dir=1,vs_mh_dir_s=1}] run scoreboard players set @s vs_mh_dir_s 0
execute as @a[scores={vs_mh_d_dir=1,vs_mh_dir_sw=1}] run scoreboard players set @s vs_mh_dir_sw 0
execute as @a[scores={vs_mh_d_dir=1,vs_mh_dir_w=1}] run scoreboard players set @s vs_mh_dir_w 0
execute as @a[scores={vs_mh_d_dir=1,vs_mh_dir_nw=1}] run scoreboard players set @s vs_mh_dir_nw 0

execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=158..203}] run scoreboard players set @s vs_mh_dir_n 1
execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=204..247}] run scoreboard players set @s vs_mh_dir_ne 1
execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=248..292}] run scoreboard players set @s vs_mh_dir_e 1
execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=293..337}] run scoreboard players set @s vs_mh_dir_se 1
execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=338..360}] run scoreboard players set @s vs_mh_dir_s 1
execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=0..22}] run scoreboard players set @s vs_mh_dir_s 1
execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=23..67}] run scoreboard players set @s vs_mh_dir_sw 1
execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=68..112}] run scoreboard players set @s vs_mh_dir_w 1
execute as @a[scores={vs_mh_d_dir=1,vs_mh_direction=113..157}] run scoreboard players set @s vs_mh_dir_nw 1