############################################################
# Description: Commands to run if the player is holding an item in either of their hands
# Creator: CreeperMagnet_
############################################################

## Charms for rings
execute as @s[nbt=!{Inventory:[{Slot:0b},{Slot:1b},{Slot:2b},{Slot:3b},{Slot:4b},{Slot:5b},{Slot:6b},{Slot:7b},{Slot:8b},{Slot:9b},{Slot:10b},{Slot:11b},{Slot:12b},{Slot:13b},{Slot:14b},{Slot:15b},{Slot:16b},{Slot:17b},{Slot:18b},{Slot:19b},{Slot:20b},{Slot:21b},{Slot:22b},{Slot:23b},{Slot:24b},{Slot:25b},{Slot:26b},{Slot:27b},{Slot:28b},{Slot:29b},{Slot:30b},{Slot:31b},{Slot:32b},{Slot:33b},{Slot:34b},{Slot:35b}]},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Charms:["attraction"]}}}]}] at @s if entity @e[type=item,distance=..5,sort=arbitrary] run function tcc:charms/attraction
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Charms:["momentum"]}}}]}] run function tcc:charms/momentum/tick


## Carrot on stick commands
execute as @s[scores={tcc.carrotstick=1..}] run function tcc:items/carrot_on_a_stick

## Mending Items
execute as @s[nbt=!{SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"soaked_golden_ring",Durability:25}}}},nbt=!{SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"soaked_iron_ring",Durability:50}}}},nbt=!{SelectedItem:{tag:{tcc:{Item:"boomerang",Durability:300}}}},nbt=!{SelectedItem:{tag:{tcc:{Item:"obsidian_mirror",Durability:20}}}},nbt=!{SelectedItem:{tag:{tcc:{Item:"wrench",Durability:200}}}},nbt=!{SelectedItem:{tag:{tcc:{Item:"obsidian_scythe",Durability:750}}}},nbt={SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Tags:["mendable"]}}}}] run function tcc:items/mend_items/mend
execute as @s[nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"soaked_golden_ring",Durability:25}}}]},nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"soaked_iron_ring",Durability:50}}}]},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Item:"boomerang",Durability:300}}}]},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Item:"obsidian_mirror",Durability:20}}}]},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Item:"wrench",Durability:200}}}]},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Item:"obsidian_scythe",Durability:750}}}]},nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Tags:["mendable"]}}}]}] run function tcc:items/mend_items/mend

## Obsidian Scythes
execute as @s[nbt={SelectedItem:{tag:{tcc:{Item:"obsidian_scythe"}}}}] run function tcc:items/obsidian_scythe/tick

# Charms

## Charm ticking
execute as @s[gamemode=!spectator,nbt={SelectedItem:{tag:{tcc:{Tags:["charmed_item_tick"]}}}}] run function tcc:charms/item_tick
