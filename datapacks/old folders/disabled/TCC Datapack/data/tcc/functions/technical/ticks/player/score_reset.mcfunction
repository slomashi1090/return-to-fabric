############################################################
# Description: Resets block breaking scores for players
# Creator: CreeperMagnet_
############################################################

scoreboard players reset @s[scores={tcc.minecoalore=1..}] tcc.minecoalore
scoreboard players reset @s[scores={tcc.mineironore=1..}] tcc.mineironore
scoreboard players reset @s[scores={tcc.minediamond=1..}] tcc.minediamond
scoreboard players reset @s[scores={tcc.minecoalore=1..}] tcc.minegoldore
scoreboard players reset @s[scores={tcc.mineemerald=1..}] tcc.mineemerald
scoreboard players reset @s[scores={tcc.minequartz=1..}] tcc.minequartz
scoreboard players reset @s[scores={tcc.minelapisore=1..}] tcc.minelapisore
scoreboard players reset @s[scores={tcc.mineredstone=1..}] tcc.mineredstone
scoreboard players reset @s[scores={tcc.logbreak1=1..}] tcc.logbreak1
scoreboard players reset @s[scores={tcc.logbreak2=1..}] tcc.logbreak2
scoreboard players reset @s[scores={tcc.logbreak3=1..}] tcc.logbreak3
scoreboard players reset @s[scores={tcc.logbreak4=1..}] tcc.logbreak4
scoreboard players reset @s[scores={tcc.logbreak5=1..}] tcc.logbreak5
scoreboard players reset @s[scores={tcc.logbreak6=1..}] tcc.logbreak6
scoreboard players reset @s[scores={tcc.slogbreak1=1..}] tcc.slogbreak1
scoreboard players reset @s[scores={tcc.slogbreak2=1..}] tcc.slogbreak2
scoreboard players reset @s[scores={tcc.slogbreak3=1..}] tcc.slogbreak3
scoreboard players reset @s[scores={tcc.slogbreak4=1..}] tcc.slogbreak4
scoreboard players reset @s[scores={tcc.slogbreak5=1..}] tcc.slogbreak5
scoreboard players reset @s[scores={tcc.slogbreak6=1..}] tcc.slogbreak6
scoreboard players reset @s[scores={tcc.barkbreak1=1..}] tcc.barkbreak1
scoreboard players reset @s[scores={tcc.barkbreak2=1..}] tcc.barkbreak2
scoreboard players reset @s[scores={tcc.barkbreak3=1..}] tcc.barkbreak3
scoreboard players reset @s[scores={tcc.barkbreak4=1..}] tcc.barkbreak4
scoreboard players reset @s[scores={tcc.barkbreak5=1..}] tcc.barkbreak5
scoreboard players reset @s[scores={tcc.barkbreak6=1..}] tcc.barkbreak6
scoreboard players reset @s[scores={tcc.minediorite=1..}] tcc.minediorite
scoreboard players reset @s[scores={tcc.minegranite=1..}] tcc.minegranite
scoreboard players reset @s[scores={tcc.mineandesite=1..}] tcc.mineandesite

scoreboard players reset @s[scores={tcc.minebeets=1..}] tcc.minebeets
scoreboard players reset @s[scores={tcc.minewheat=1..}] tcc.minewheat
scoreboard players reset @s[scores={tcc.minecarrots=1..}] tcc.minecarrots
scoreboard players reset @s[scores={tcc.minepotatoes=1..}] tcc.minepotatoes
