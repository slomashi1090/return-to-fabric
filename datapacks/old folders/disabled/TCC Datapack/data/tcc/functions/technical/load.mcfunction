############################################################
# Description: Adds all scoreboards and sets up everything
# Creator: CreeperMagnet_
############################################################


tellraw @a {"translate":"tcc.tellraws.reloaded"}
gamerule logAdminCommands false
gamerule commandBlockOutput false


scoreboard objectives add tcc.carrotstick minecraft.used:minecraft.carrot_on_a_stick


scoreboard objectives add tcc.boomerang dummy
scoreboard objectives add tcc.boomerangu dummy
scoreboard objectives add tcc.boomerangm1 dummy
scoreboard objectives add tcc.boomerangm2 dummy

scoreboard objectives add tcc.wrench dummy
scoreboard objectives add tcc.logcount dummy

scoreboard objectives add tcc.logbreak1 minecraft.mined:minecraft.acacia_log
scoreboard objectives add tcc.logbreak2 minecraft.mined:minecraft.dark_oak_log
scoreboard objectives add tcc.logbreak3 minecraft.mined:minecraft.birch_log
scoreboard objectives add tcc.logbreak4 minecraft.mined:minecraft.oak_log
scoreboard objectives add tcc.logbreak5 minecraft.mined:minecraft.spruce_log
scoreboard objectives add tcc.logbreak6 minecraft.mined:minecraft.jungle_log
scoreboard objectives add tcc.slogbreak1 minecraft.mined:minecraft.stripped_acacia_log
scoreboard objectives add tcc.slogbreak2 minecraft.mined:minecraft.stripped_dark_oak_log
scoreboard objectives add tcc.slogbreak3 minecraft.mined:minecraft.stripped_birch_log
scoreboard objectives add tcc.slogbreak4 minecraft.mined:minecraft.stripped_oak_log
scoreboard objectives add tcc.slogbreak5 minecraft.mined:minecraft.stripped_spruce_log
scoreboard objectives add tcc.slogbreak6 minecraft.mined:minecraft.stripped_jungle_log
scoreboard objectives add tcc.barkbreak1 minecraft.mined:minecraft.acacia_wood
scoreboard objectives add tcc.barkbreak2 minecraft.mined:minecraft.dark_oak_wood
scoreboard objectives add tcc.barkbreak3 minecraft.mined:minecraft.birch_wood
scoreboard objectives add tcc.barkbreak4 minecraft.mined:minecraft.oak_wood
scoreboard objectives add tcc.barkbreak5 minecraft.mined:minecraft.spruce_wood
scoreboard objectives add tcc.barkbreak6 minecraft.mined:minecraft.jungle_wood
scoreboard objectives add tcc.sbarkbreak1 minecraft.mined:minecraft.stripped_acacia_wood
scoreboard objectives add tcc.sbarkbreak2 minecraft.mined:minecraft.stripped_dark_oak_wood
scoreboard objectives add tcc.sbarkbreak3 minecraft.mined:minecraft.stripped_birch_wood
scoreboard objectives add tcc.sbarkbreak4 minecraft.mined:minecraft.stripped_oak_wood
scoreboard objectives add tcc.sbarkbreak5 minecraft.mined:minecraft.stripped_spruce_wood
scoreboard objectives add tcc.sbarkbreak6 minecraft.mined:minecraft.stripped_jungle_wood
scoreboard objectives add tcc.mineironore minecraft.mined:minecraft.iron_ore
scoreboard objectives add tcc.minegoldore minecraft.mined:minecraft.gold_ore
scoreboard objectives add tcc.minediamond minecraft.mined:minecraft.diamond_ore
scoreboard objectives add tcc.minecoalore minecraft.mined:minecraft.coal_ore
scoreboard objectives add tcc.mineemerald minecraft.mined:minecraft.emerald_ore
scoreboard objectives add tcc.minequartz minecraft.mined:minecraft.nether_quartz_ore
scoreboard objectives add tcc.mineredstone minecraft.mined:minecraft.redstone_ore
scoreboard objectives add tcc.minelapisore minecraft.mined:minecraft.lapis_ore
scoreboard objectives add tcc.minediorite minecraft.mined:minecraft.diorite
scoreboard objectives add tcc.mineandesite minecraft.mined:minecraft.andesite
scoreboard objectives add tcc.minegranite minecraft.mined:minecraft.granite
scoreboard objectives add tcc.minewheat minecraft.mined:minecraft.wheat
scoreboard objectives add tcc.minebeets minecraft.mined:minecraft.beetroots
scoreboard objectives add tcc.minecarrots minecraft.mined:minecraft.carrots
scoreboard objectives add tcc.minepotatoes minecraft.mined:minecraft.potatoes
scoreboard objectives add tcc.minechorus1 minecraft.mined:minecraft.chorus_plant
scoreboard objectives add tcc.minechorus2 minecraft.mined:minecraft.chorus_flower

scoreboard objectives add tcc.random dummy
scoreboard objectives add tcc.math dummy
scoreboard objectives add tcc.math2 dummy
scoreboard objectives add tcc.installed dummy
scoreboard players set @a tcc.installed 1

scoreboard objectives add tcc.useelytra minecraft.custom:minecraft.aviate_one_cm
scoreboard objectives add tcc.shifttime minecraft.custom:minecraft.sneak_time
scoreboard objectives add tcc.hunger food
scoreboard objectives add tcc.health health
scoreboard objectives add tcc.run minecraft.custom:minecraft.sprint_one_cm
scoreboard objectives add tcc.walk minecraft.custom:minecraft.walk_one_cm
scoreboard objectives add tcc.crouch minecraft.custom:minecraft.crouch_one_cm
scoreboard objectives add tcc.runtime dummy

scoreboard objectives add tcc.current_xp xp
scoreboard objectives add tcc.old_xp dummy
