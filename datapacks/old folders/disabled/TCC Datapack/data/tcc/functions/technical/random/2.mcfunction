############################################################
# Description: Generates a random number, either 1 or 2
# Creator: CreeperMagnet_
############################################################

scoreboard players reset @s tcc.random
summon area_effect_cloud ~ ~ ~ {Tags:["tcc.random"],Invisible:1b,Marker:1b}
execute store result score @s tcc.random run data get entity @e[type=area_effect_cloud,tag=tcc.random,limit=1] UUIDMost 0.0000000000000001

execute as @s[scores={tcc.random=1..1099}] run scoreboard players set @s tcc.random 2
execute as @s[scores={tcc.random=-1199..0}] run scoreboard players set @s tcc.random 1
execute as @s unless entity @s[scores={tcc.random=1..2}] run scoreboard players set @s tcc.random 2

kill @e[tag=tcc.random,type=area_effect_cloud]