############################################################
# Description: Generates a random number, either 1 or 2
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/4
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome1.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome1.2
execute as @s[scores={tcc.random=3}] run tag @s add tcc.random.outcome1.3
execute as @s[scores={tcc.random=4}] run tag @s add tcc.random.outcome1.4
function tcc:technical/random/2
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome2.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome2.2
scoreboard players reset @s tcc.random

execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 1
execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 2
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 3
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 4
execute as @s[tag=tcc.random.outcome1.3,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 5
execute as @s[tag=tcc.random.outcome1.3,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 6
execute as @s[tag=tcc.random.outcome1.4,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 7
execute as @s[tag=tcc.random.outcome1.4,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 8
scoreboard players add @s tcc.random 8

tag @s remove tcc.random.outcome1.1
tag @s remove tcc.random.outcome1.2
tag @s remove tcc.random.outcome1.3
tag @s remove tcc.random.outcome1.4
tag @s remove tcc.random.outcome2.1
tag @s remove tcc.random.outcome2.2