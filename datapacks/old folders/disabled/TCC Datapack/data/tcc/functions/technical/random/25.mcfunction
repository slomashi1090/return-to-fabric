############################################################
# Description: Generates a random number, from 1 to 25
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/32
execute as @s[scores={tcc.random=26..32}] run function tcc:technical/random/25
