############################################################
# Description: Commands to run if you have an item with sturdiness in your hand
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/charms/sturdiness
effect give @s mining_fatigue 10 3 true
effect give @s weakness 10 3 true
execute at @s run playsound minecraft:entity.item.break player @s[distance=..16]
execute at @s run playsound minecraft:entity.evoker.prepare_attack player @a[distance=..16] ~ ~ ~ 16 1.5