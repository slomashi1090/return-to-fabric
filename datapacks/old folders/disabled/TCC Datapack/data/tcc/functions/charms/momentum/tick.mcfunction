############################################################
# Description: Makes the momentum function work
# Creator: CreeperMagnet_ & dragoncommands
############################################################

execute as @s[scores={tcc.run=10..}] run scoreboard players add @s tcc.runtime 1
execute as @s[scores={tcc.walk=1..}] run function tcc:charms/momentum/reset
execute as @s[scores={tcc.crouch=1..}] run function tcc:charms/momentum/reset
execute if block ~ ~-0.87 ~ minecraft:soul_sand run function tcc:charms/momentum/reset
execute if block ~ ~ ~ #tcc:momentum_stop run function tcc:charms/momentum/reset

execute as @s[scores={tcc.runtime=4000..}] run advancement grant @s only tcc:minecraft/adventure/momentum
execute as @s[scores={tcc.runtime=1..10000}] store result entity @s Inventory[{Slot:-106b}].tag.tcc.momentum_amount double 0.04 run scoreboard players get @s tcc.runtime
execute as @s[scores={tcc.runtime=1..10000}] store result entity @s Inventory[{Slot:-106b}].tag.AttributeModifiers[0].Amount double 0.01 run data get entity @s Inventory[{Slot:-106b}].tag.tcc.momentum_amount
