############################################################
# Description: Makes the veining enchantment find ore around
# Creator: CreeperMagnet_
############################################################

particle block minecraft:diorite ~ ~ ~ 0.5 0.5 0.5 1 1 force
execute as @a[scores={tcc.minediorite=1..},distance=..10] run loot spawn ~ ~ ~ mine ~ ~ ~ mainhand
setblock ~ ~ ~ air
execute positioned ~ ~1 ~ if block ~ ~ ~ diorite if entity @a[scores={tcc.minediorite=1..},distance=..10] run function tcc:charms/veining/diorite/loop
execute positioned ~ ~-1 ~ if block ~ ~ ~ diorite if entity @a[scores={tcc.minediorite=1..},distance=..10] run function tcc:charms/veining/diorite/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ diorite if entity @a[scores={tcc.minediorite=1..},distance=..10] run function tcc:charms/veining/diorite/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ diorite if entity @a[scores={tcc.minediorite=1..},distance=..10] run function tcc:charms/veining/diorite/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ diorite if entity @a[scores={tcc.minediorite=1..},distance=..10] run function tcc:charms/veining/diorite/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ diorite if entity @a[scores={tcc.minediorite=1..},distance=..10] run function tcc:charms/veining/diorite/loop