############################################################
# Description: Makes the timbering enchantment find logs above it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:birch_log ~ ~ ~ 0.5 0.5 0.5 1 1 force
setblock ~ ~ ~ air
scoreboard players add @s tcc.logcount 1
execute positioned ~ ~1 ~ if block ~ ~ ~ birch_log if entity @a[scores={tcc.logbreak3=1..},distance=..10] run function tcc:charms/timbering/birch_log/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ birch_log if entity @a[scores={tcc.logbreak3=1..},distance=..10] run function tcc:charms/timbering/birch_log/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ birch_log if entity @a[scores={tcc.logbreak3=1..},distance=..10] run function tcc:charms/timbering/birch_log/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ birch_log if entity @a[scores={tcc.logbreak3=1..},distance=..10] run function tcc:charms/timbering/birch_log/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ birch_log if entity @a[scores={tcc.logbreak3=1..},distance=..10] run function tcc:charms/timbering/birch_log/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ birch_log if entity @a[scores={tcc.logbreak3=1..},distance=..10] run function tcc:charms/timbering/birch_log/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ birch_log if entity @a[scores={tcc.logbreak3=1..},distance=..10] run function tcc:charms/timbering/birch_log/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ birch_log if entity @a[scores={tcc.logbreak3=1..},distance=..10] run function tcc:charms/timbering/birch_log/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ birch_log if entity @a[scores={tcc.logbreak3=1..},distance=..10] run function tcc:charms/timbering/birch_log/loop
execute unless block ~ ~1 ~-1 birch_log unless block ~ ~1 ~1 birch_log unless block ~ ~1 ~ birch_log unless block ~1 ~1 ~ birch_log unless block ~-1 ~1 ~ birch_log unless block ~1 ~ ~ birch_log unless block ~-1 ~ ~ birch_log unless block ~ ~ ~1 birch_log unless block ~ ~ ~-1 birch_log run function tcc:charms/timbering/birch_log/count