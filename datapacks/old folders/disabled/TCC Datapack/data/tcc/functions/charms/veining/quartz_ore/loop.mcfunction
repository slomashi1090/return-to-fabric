############################################################
# Description: Makes the veining enchantment find ore around
# Creator: CreeperMagnet_
############################################################

particle block minecraft:nether_quartz_ore ~ ~ ~ 0.5 0.5 0.5 1 1 force
execute as @a[scores={tcc.minequartz=1..},distance=..10] run loot spawn ~ ~ ~ mine ~ ~ ~ mainhand
setblock ~ ~ ~ air
execute positioned ~ ~1 ~ if block ~ ~ ~ nether_quartz_ore if entity @a[scores={tcc.minequartz=1..},distance=..10] run function tcc:charms/veining/quartz_ore/loop
execute positioned ~ ~-1 ~ if block ~ ~ ~ nether_quartz_ore if entity @a[scores={tcc.minequartz=1..},distance=..10] run function tcc:charms/veining/quartz_ore/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ nether_quartz_ore if entity @a[scores={tcc.minequartz=1..},distance=..10] run function tcc:charms/veining/quartz_ore/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ nether_quartz_ore if entity @a[scores={tcc.minequartz=1..},distance=..10] run function tcc:charms/veining/quartz_ore/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ nether_quartz_ore if entity @a[scores={tcc.minequartz=1..},distance=..10] run function tcc:charms/veining/quartz_ore/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ nether_quartz_ore if entity @a[scores={tcc.minequartz=1..},distance=..10] run function tcc:charms/veining/quartz_ore/loop