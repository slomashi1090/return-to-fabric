############################################################
# Description: Makes the reaping enchantment find potatoes around it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:potatoes ~ ~ ~ 0.5 0.5 0.5 1 1 force
execute as @p[scores={tcc.minepotatoes=1..},distance=..4] run loot spawn ~ ~ ~ mine ~ ~ ~ mainhand
setblock ~ ~ ~ air
execute positioned ~ ~1 ~ if block ~ ~ ~ minecraft:potatoes[age=7] if entity @a[scores={tcc.minepotatoes=1..},distance=..4] run function tcc:charms/reaping/potatoes/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ minecraft:potatoes[age=7] if entity @a[scores={tcc.minepotatoes=1..},distance=..4] run function tcc:charms/reaping/potatoes/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ minecraft:potatoes[age=7] if entity @a[scores={tcc.minepotatoes=1..},distance=..4] run function tcc:charms/reaping/potatoes/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ minecraft:potatoes[age=7] if entity @a[scores={tcc.minepotatoes=1..},distance=..4] run function tcc:charms/reaping/potatoes/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ minecraft:potatoes[age=7] if entity @a[scores={tcc.minepotatoes=1..},distance=..4] run function tcc:charms/reaping/potatoes/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ minecraft:potatoes[age=7] if entity @a[scores={tcc.minepotatoes=1..},distance=..4] run function tcc:charms/reaping/potatoes/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ minecraft:potatoes[age=7] if entity @a[scores={tcc.minepotatoes=1..},distance=..4] run function tcc:charms/reaping/potatoes/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ minecraft:potatoes[age=7] if entity @a[scores={tcc.minepotatoes=1..},distance=..4] run function tcc:charms/reaping/potatoes/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ minecraft:potatoes[age=7] if entity @a[scores={tcc.minepotatoes=1..},distance=..4] run function tcc:charms/reaping/potatoes/loop
