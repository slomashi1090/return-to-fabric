############################################################
# Description: Commands for the attraction charm for golden rings
# Creator: CreeperMagnet_
############################################################

execute store result score @s tcc.math run data get entity @s UUIDMost 0.0000000001

execute as @e[type=item,distance=..5] store result score @s tcc.math run data get entity @s Thrower.M 0.0000000001

execute as @e[type=item,distance=..5] at @s if score @s tcc.math = @p[distance=..5,nbt={Inventory:[{Slot:-106b,tag:{tcc:{Charms:["attraction"],Item:"golden_ring"}}}]},tag=!tcc.charms.cannot_attract] tcc.math run tag @p[distance=..5,nbt={Inventory:[{Slot:-106b,tag:{tcc:{Charms:["attraction"],Item:"golden_ring"}}}]},tag=!tcc.charms.cannot_attract] add tcc.charms.cannot_attract
execute as @e[type=item,distance=..5] if entity @a[distance=..5,nbt={Inventory:[{Slot:-106b,tag:{tcc:{Charms:["attraction"],Item:"golden_ring"}}}]},tag=!tcc.charms.cannot_attract] run data merge entity @s {PickupDelay:0s}
execute as @e[type=item,distance=..5] at @s run tp @s @p[distance=..5,nbt={Inventory:[{Slot:-106b,tag:{tcc:{Charms:["attraction"],Item:"golden_ring"}}}]},tag=!tcc.charms.cannot_attract]
tag @a[tag=tcc.charms.cannot_attract] remove tcc.charms.cannot_attract

scoreboard players reset @s tcc.math
scoreboard players reset @e[type=item,distance=..5] tcc.math
