############################################################
# Description: Makes the timbering enchantment find logs above it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:oak_log ~ ~ ~ 0.5 0.5 0.5 1 1 force
setblock ~ ~ ~ air
scoreboard players add @s tcc.logcount 1
execute positioned ~ ~1 ~ if block ~ ~ ~ oak_log if entity @a[scores={tcc.logbreak4=1..},distance=..10] run function tcc:charms/timbering/oak_log/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ oak_log if entity @a[scores={tcc.logbreak4=1..},distance=..10] run function tcc:charms/timbering/oak_log/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ oak_log if entity @a[scores={tcc.logbreak4=1..},distance=..10] run function tcc:charms/timbering/oak_log/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ oak_log if entity @a[scores={tcc.logbreak4=1..},distance=..10] run function tcc:charms/timbering/oak_log/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ oak_log if entity @a[scores={tcc.logbreak4=1..},distance=..10] run function tcc:charms/timbering/oak_log/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ oak_log if entity @a[scores={tcc.logbreak4=1..},distance=..10] run function tcc:charms/timbering/oak_log/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ oak_log if entity @a[scores={tcc.logbreak4=1..},distance=..10] run function tcc:charms/timbering/oak_log/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ oak_log if entity @a[scores={tcc.logbreak4=1..},distance=..10] run function tcc:charms/timbering/oak_log/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ oak_log if entity @a[scores={tcc.logbreak4=1..},distance=..10] run function tcc:charms/timbering/oak_log/loop
execute unless block ~ ~1 ~-1 oak_log unless block ~ ~1 ~1 oak_log unless block ~ ~1 ~ oak_log unless block ~1 ~1 ~ oak_log unless block ~-1 ~1 ~ oak_log unless block ~1 ~ ~ oak_log unless block ~-1 ~ ~ oak_log unless block ~ ~ ~1 oak_log unless block ~ ~ ~-1 oak_log run function tcc:charms/timbering/oak_log/count