############################################################
# Description: Makes end crystal beam choose its targets
# Creator: CreeperMagnet_
############################################################

execute as @s[tag=!tcc.end_crystal.used] if entity @p[distance=..30,tag=!tcc.end_crystal.player_used,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]},gamemode=!spectator] run function tcc:entities/end_crystal/target
execute as @s[tag=!tcc.end_crystal.used] run tag @p[distance=..30,tag=!tcc.end_crystal.player_used,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]},gamemode=!spectator] add tcc.end_crystal.player_used
tag @s[tag=!tcc.end_crystal.used] add tcc.end_crystal.used
