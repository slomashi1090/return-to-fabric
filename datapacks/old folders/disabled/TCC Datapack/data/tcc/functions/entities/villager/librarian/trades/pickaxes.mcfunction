############################################################
# Description: Chooses a random kind of item to add to charm trades
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/5
execute as @s[scores={tcc.random=1}] run function tcc:entities/villager/librarian/trades/pickaxes/diamond_pickaxe
execute as @s[scores={tcc.random=2}] run function tcc:entities/villager/librarian/trades/pickaxes/golden_pickaxe
execute as @s[scores={tcc.random=3}] run function tcc:entities/villager/librarian/trades/pickaxes/iron_pickaxe
execute as @s[scores={tcc.random=4}] run function tcc:entities/villager/librarian/trades/pickaxes/wooden_pickaxe
execute as @s[scores={tcc.random=5}] run function tcc:entities/villager/librarian/trades/pickaxes/stone_pickaxe
