############################################################
# Description: Adds a charmed item trade to a villager
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/8
execute as @s[scores={tcc.random=1}] run function tcc:entities/villager/librarian/trades/pickaxes
execute as @s[scores={tcc.random=2}] run function tcc:entities/villager/librarian/trades/axes
execute as @s[scores={tcc.random=3}] run function tcc:entities/villager/librarian/trades/shovels
execute as @s[scores={tcc.random=4}] run function tcc:entities/villager/librarian/trades/swords
execute as @s[scores={tcc.random=5}] run function tcc:entities/villager/librarian/trades/rings
execute as @s[scores={tcc.random=6}] run function tcc:entities/villager/librarian/trades/boomerang
execute as @s[scores={tcc.random=7}] run function tcc:entities/villager/librarian/trades/saddle
execute as @s[scores={tcc.random=8}] run function tcc:entities/villager/librarian/trades/obsidian_scythe
