############################################################
# Description: Sets a random sword charm
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/3
execute as @s[scores={tcc.random=1}] run data modify entity @s Offers.Recipes append value {maxUses:8,buyB:{id:"minecraft:wooden_sword",Count:1b},buy:{id:"minecraft:emerald",Count:1b},sell:{Count:1b,id:"minecraft:wooden_sword",tag:{tcc:{Charms:["learning"]},CustomModelData:1,display:{Lore:["{\"color\":\"dark_green\",\"italic\":\"false\",\"translate\":\"tcc.lore.charms.prefix\",\"with\":[{\"translate\":\"tcc.lore.charms.learning\"}]}"]}}},xp:1,uses:0,priceMultipler:0.2f,specialPrice:0,demand:0}
execute as @s[scores={tcc.random=2}] run data modify entity @s Offers.Recipes append value {maxUses:8,buyB:{id:"minecraft:wooden_sword",Count:1b},buy:{id:"minecraft:emerald",Count:1b},sell:{Count:1b,id:"minecraft:wooden_sword",tag:{tcc:{Hexes:["midas_touch"]},CustomModelData:2,display:{Lore:["{\"color\":\"dark_red\",\"italic\":\"false\",\"translate\":\"tcc.lore.hexes.prefix\",\"with\":[{\"translate\":\"tcc.lore.hexes.midas_touch\"}]}"]}}},xp:1,uses:0,priceMultipler:0.2f,specialPrice:0,demand:0}
execute as @s[scores={tcc.random=3}] run data modify entity @s Offers.Recipes append value {maxUses:8,buyB:{id:"minecraft:wooden_sword",Count:1b},buy:{id:"minecraft:emerald",Count:1b},sell:{Count:1b,id:"minecraft:wooden_sword",tag:{tcc:{Charms:["learning"],Hexes:["midas_touch"]},CustomModelData:3,display:{Lore:["{\"color\":\"dark_green\",\"italic\":\"false\",\"translate\":\"tcc.lore.charms.prefix\",\"with\":[{\"translate\":\"tcc.lore.charms.learning\"}]}","{\"color\":\"dark_red\",\"italic\":\"false\",\"translate\":\"tcc.lore.hexes.prefix\",\"with\":[{\"translate\":\"tcc.lore.hexes.midas_touch\"}]}"]}}},xp:1,uses:0,priceMultipler:0.2f,specialPrice:0,demand:0}
scoreboard players reset @s tcc.random
