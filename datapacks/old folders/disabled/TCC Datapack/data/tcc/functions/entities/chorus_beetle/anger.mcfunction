############################################################
# Description: Angers chorus beetles when you mine chorus
# Creator: CreeperMagnet_
############################################################

execute as @e[tag=tcc.chorus_beetle,type=zombie_pigman,distance=..50,sort=arbitrary] run data merge entity @s {Anger:500s}
execute if entity @e[tag=tcc.chorus_beetle,type=zombie_pigman,distance=..50,sort=arbitrary] run playsound minecraft:entity.enderman.scream hostile @s ~ ~ ~ 1000 0
scoreboard players reset @a[scores={tcc.minechorus1=1..}] tcc.minechorus1
scoreboard players reset @a[scores={tcc.minechorus2=1..}] tcc.minechorus2
