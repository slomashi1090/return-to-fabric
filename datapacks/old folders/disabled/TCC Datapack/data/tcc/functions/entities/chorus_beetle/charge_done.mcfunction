############################################################
# Description: Stops the chorus beetle after it charges
# Creator: CreeperMagnet_
############################################################

playsound minecraft:entity.endermite.ambient hostile @a[distance=..16] ~ ~ ~ 16 0
replaceitem entity @s weapon.mainhand air
data merge entity @s {Anger:0}
