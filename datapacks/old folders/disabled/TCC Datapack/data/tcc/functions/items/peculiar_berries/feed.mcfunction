############################################################
# Description: Makes the peculiar berries work
# Creator: CreeperMagnet_
############################################################

advancement grant @s only tcc:minecraft/husbandry/peculiar_berries
execute as @s[gamemode=!creative,nbt={Inventory:[{tag:{tcc:{Item:"peculiar_berries"}},Slot:-106b}]},nbt=!{SelectedItem:{tag:{tcc:{Item:"peculiar_berries"}}}}] run replaceitem entity @s weapon.offhand air
execute as @s[gamemode=!creative,nbt={SelectedItem:{tag:{tcc:{Item:"peculiar_berries"}}}}] run replaceitem entity @s weapon.mainhand air
data merge entity @e[type=chicken,tag=!tcc.peculiar_berries.chicken,dx=0,dy=0,dz=0,limit=1,nbt={Age:0}] {DeathLootTable:"tcc:entities/peculiar_berries_chicken",Tags:["tcc.peculiar_berries.chicken"]}
execute as @e[type=chicken,tag=tcc.peculiar_berries.chicken,dx=0,dy=0,dz=0,limit=1] at @s run particle minecraft:enchant ~ ~1 ~ 0 0 0 1 100 force @a
playsound minecraft:entity.horse.eat neutral @a[distance=..16] ~ ~ ~ 16 1.2
