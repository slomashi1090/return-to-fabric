############################################################
# Description: Damages the obsidian scythe
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/items/obsidian_scythe_damage
execute as @s[nbt={SelectedItem:{tag:{Enchantments:[{lvl:1s,id:"minecraft:unbreaking"}]}}}] run function tcc:technical/random/2
execute as @s[nbt={SelectedItem:{tag:{Enchantments:[{lvl:2s,id:"minecraft:unbreaking"}]}}}] run function tcc:technical/random/3
execute as @s[nbt={SelectedItem:{tag:{Enchantments:[{lvl:3s,id:"minecraft:unbreaking"}]}}}] run function tcc:technical/random/4
execute as @s[nbt=!{SelectedItem:{tag:{Enchantments:[{id:"minecraft:unbreaking"}]}}}] run scoreboard players set @s tcc.random 1

execute as @s[scores={tcc.random=1},nbt={SelectedItem:{tag:{tcc:{Durability:1}}}}] run playsound minecraft:entity.item.break player @a[distance=..16]
execute as @s[scores={tcc.random=1},nbt={SelectedItem:{tag:{tcc:{Durability:1}}}}] run particle item minecraft:carrot_on_a_stick{CustomModelData:13} ~ ~1 ~ 0.2 0.2 0.2 0.1 10 force
execute as @s[scores={tcc.random=1},nbt={SelectedItem:{tag:{tcc:{Durability:1}}}}] run replaceitem entity @s weapon.mainhand air

execute as @s[scores={tcc.random=1},nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"obsidian_scythe"}}}}] store result score @s tcc.math run data get entity @s SelectedItem.tag.tcc.Durability 1
execute as @s[scores={tcc.random=1},nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"obsidian_scythe"}}}}] run scoreboard players remove @s tcc.math 1
execute as @s[scores={tcc.random=1},nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"obsidian_scythe"}}}}] store result entity @s SelectedItem.tag.tcc.Durability int 1 run scoreboard players get @s tcc.math
execute as @s[scores={tcc.random=1},nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"obsidian_scythe"}}}}] run scoreboard players reset @s tcc.math

execute as @s[scores={tcc.random=1},nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"obsidian_scythe"}}}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/obsidian_scythe/mainhand/store
execute as @s[scores={tcc.random=1},nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"obsidian_scythe"}}}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/obsidian_scythe/mainhand/reorder

scoreboard players reset @s tcc.random
