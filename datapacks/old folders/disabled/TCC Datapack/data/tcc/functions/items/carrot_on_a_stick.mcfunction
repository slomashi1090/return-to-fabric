############################################################
# Description: All commands to run involving carrots on sticks
# Creator: CreeperMagnet_
############################################################

# Cracked Nether Star
execute unless entity @s[nbt=!{SelectedItem:{tag:{tcc:{Item:"cracked_nether_star"}}}},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Item:"cracked_nether_star"}}}]}] as @s[nbt=!{ActiveEffects:[{Ambient:1b}]}] at @s run function tcc:items/cracked_nether_star

# Witch Hats
execute unless entity @s[nbt=!{SelectedItem:{tag:{tcc:{Item:"witch_hat"}}}},nbt=!{Inventory:[{tag:{tcc:{Item:"witch_hat"}},Slot:-106b}]}] as @s[nbt=!{Inventory:[{Slot:103b}]}] at @s run function tcc:items/witch_hat

# Wrenches
execute unless entity @s[nbt=!{SelectedItem:{tag:{tcc:{Item:"wrench"}}}},nbt=!{Inventory:[{tag:{tcc:{Item:"wrench"}},Slot:-106b}]}] at @s run function tcc:items/wrench/item

# Soaked Rings
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"soaked_iron_ring"}}}]}] run function tcc:items/potion_items/soaked_rings/iron_ring/use
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"soaked_golden_ring"}}}]}] run function tcc:items/potion_items/soaked_rings/golden_ring/use

# Obsidian Mirror
execute unless entity @s[nbt=!{SelectedItem:{tag:{tcc:{Item:"obsidian_mirror"}}}},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Item:"obsidian_mirror"}}}]}] unless entity @s[nbt=!{ActiveEffects:[{Id:2b}]},nbt=!{ActiveEffects:[{Id:4b}]},nbt=!{ActiveEffects:[{Id:9b}]},nbt=!{ActiveEffects:[{Id:15b}]},nbt=!{ActiveEffects:[{Id:17b}]},nbt=!{ActiveEffects:[{Id:18b}]},nbt=!{ActiveEffects:[{Id:19b}]},nbt=!{ActiveEffects:[{Id:20b}]},nbt=!{ActiveEffects:[{Id:24b}]},nbt=!{ActiveEffects:[{Id:25b}]},nbt=!{ActiveEffects:[{Id:27b}]}] run function tcc:items/obsidian_mirror/flip


# Peculiar Berries
execute unless entity @s[nbt=!{SelectedItem:{tag:{tcc:{Item:"peculiar_berries"}}}},nbt=!{Inventory:[{tag:{tcc:{Item:"peculiar_berries"}},Slot:-106b}]}] at @s run function tcc:items/peculiar_berries/item

# Quest Books
execute as @s[tag=!tcc.player.quest.healer.started,tag=!tcc.player.quest.mage.started,tag=!tcc.player.quest.warrior.started,tag=!tcc.player.quest.rogue.started] unless entity @s[nbt=!{SelectedItem:{tag:{tcc:{Item:"quest_book"}}}},nbt=!{Inventory:[{tag:{tcc:{Item:"quest_book"}},Slot:-106b}]}] at @s run function tcc:items/quest_book/item

# Boomerangs
execute as @s[x_rotation=-50..50,nbt={SelectedItem:{tag:{tcc:{Charms:["bouncing"]}}}},nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"boomerang"}}}}] at @s anchored eyes if block ^ ^ ^1 #tcc:not_solid if block ^ ^ ^2 #tcc:not_solid if block ^ ^ ^3 #tcc:not_solid if block ^ ^ ^4 #tcc:not_solid if block ^ ^ ^5 #tcc:not_solid run advancement grant @s only tcc:minecraft/adventure/ricochet
execute as @s[x_rotation=-50..50,nbt=!{SelectedItem:{tag:{tcc:{Charms:["flowing"]}}}},nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"boomerang"}}}}] at @s anchored eyes if block ^ ^ ^1 #tcc:not_solid if block ^ ^ ^2 #tcc:not_solid if block ^ ^ ^3 #tcc:not_solid if block ^ ^ ^4 #tcc:not_solid if block ^ ^ ^5 #tcc:not_solid run function tcc:items/boomerang/item/throw
execute as @s[x_rotation=-50..50,nbt={SelectedItem:{tag:{tcc:{Charms:["flowing"]}}}},nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"boomerang"}}}}] at @s anchored eyes if block ^ ^ ^1 #tcc:boomerang_flowing if block ^ ^ ^2 #tcc:boomerang_flowing if block ^ ^ ^3 #tcc:boomerang_flowing if block ^ ^ ^4 #tcc:boomerang_flowing if block ^ ^ ^5 #tcc:boomerang_flowing run function tcc:items/boomerang/item/throw


# Chorus Beetle Horn
execute as @s[nbt=!{ActiveEffects:[{Id:1b,Amplifier:4b,ShowParticles:0b}]},nbt={SelectedItem:{tag:{tcc:{Item:"chorus_beetle_horn"}}}}] at @s run function tcc:items/chorus_beetle_horn

# Reset
execute as @s[scores={tcc.carrotstick=1..}] run scoreboard players reset @s tcc.carrotstick
