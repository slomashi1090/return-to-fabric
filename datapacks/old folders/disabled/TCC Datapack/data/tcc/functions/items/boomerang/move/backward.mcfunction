############################################################
# Description: Moves the boomerang backward
# Creator: CreeperMagnet_
############################################################


#say going backwards
scoreboard players add @s tcc.boomerangm2 0

## Moving
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]}] at @s if block ^ ^ ^-0.05 #tcc:not_solid unless entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run tp @s ^ ^ ^-0.05
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]}] at @s unless block ^ ^ ^-0.05 #tcc:not_solid positioned ^ ^ ^0.5 run function tcc:items/boomerang/hit_block
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]}] at @s positioned ^ ^ ^-0.05 if entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] positioned ^ ^ ^0.5 run function tcc:items/boomerang/hit_block

execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]}] at @s if block ^ ^ ^-0.05 #tcc:boomerang_flowing unless entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run tp @s ^ ^ ^-0.05
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]}] at @s unless block ^ ^ ^-0.05 #tcc:boomerang_flowing positioned ^ ^ ^0.5 run function tcc:items/boomerang/hit_block
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]}] at @s positioned ^ ^ ^-0.05 if entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] positioned ^ ^ ^0.5 run function tcc:items/boomerang/hit_block

## Seeking
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]}] at @s run function tcc:items/boomerang/charms/seeking/track

## Seeking Moving
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=!tcc.boomerang.charms.seeking.track.failed] at @s if block ^ ^ ^0.05 #tcc:not_solid unless entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run tp @s ^ ^ ^0.05
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=tcc.boomerang.charms.seeking.track.failed,tag=!tcc.boomerang.tracked_player] at @s if block ^ ^ ^-0.05 #tcc:not_solid unless entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run tp @s ^ ^ ^-0.05

execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=!tcc.boomerang.charms.seeking.track.failed] at @s unless block ^ ^ ^0.05 #tcc:not_solid positioned ^ ^ ^-0.5 run function tcc:items/boomerang/hit_block
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=tcc.boomerang.charms.seeking.track.failed,tag=!tcc.boomerang.tracked_player] at @s unless block ^ ^ ^-0.05 #tcc:not_solid positioned ^ ^ ^0.5 run function tcc:items/boomerang/hit_block


execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=!tcc.boomerang.charms.seeking.track.failed] at @s if block ^ ^ ^0.05 #tcc:boomerang_flowing unless entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run tp @s ^ ^ ^0.05
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=tcc.boomerang.charms.seeking.track.failed,tag=!tcc.boomerang.tracked_player] at @s if block ^ ^ ^-0.05 #tcc:boomerang_flowing unless entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run tp @s ^ ^ ^-0.05

execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=!tcc.boomerang.charms.seeking.track.failed] at @s unless block ^ ^ ^0.05 #tcc:boomerang_flowing positioned ^ ^ ^-0.5 run function tcc:items/boomerang/hit_block
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=tcc.boomerang.charms.seeking.track.failed,tag=!tcc.boomerang.tracked_player] at @s unless block ^ ^ ^-0.05 #tcc:boomerang_flowing positioned ^ ^ ^0.5 run function tcc:items/boomerang/hit_block

execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=!tcc.boomerang.charms.seeking.track.failed] at @s positioned ^ ^ ^0.05 if entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] positioned ^ ^ ^-0.5 run function tcc:items/boomerang/hit_block
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]},tag=tcc.boomerang.charms.seeking.track.failed,tag=!tcc.boomerang.tracked_player] at @s positioned ^ ^ ^-0.05 if entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] positioned ^ ^ ^0.5 run function tcc:items/boomerang/hit_block



execute as @s[tag=tcc.boomerang.charms.seeking.track.failed,tag=tcc.boomerang.tracked_player] run function tcc:items/boomerang/time_out

## Pickup
execute as @s[scores={tcc.boomerang=..119},tag=tcc.boomerang] as @a[distance=..2,limit=1,sort=nearest,gamemode=!spectator] at @s positioned ~ ~0.81 ~ if entity @e[tag=tcc.boomerang,distance=..1] run function tcc:items/boomerang/pickup


## Hurting
execute as @e[type=!#tcc:boomerang_damage/ignore,nbt={HurtTime:0s},distance=..3,tag=!tcc.boomerang.player.uuid_match] at @s run function tcc:items/boomerang/hurt/discern


execute as @s[scores={tcc.boomerangm2=0..19}] run scoreboard players add @s tcc.boomerangm2 1
execute as @s[scores={tcc.boomerangm2=0..19}] run function tcc:items/boomerang/move/backward
execute as @s[scores={tcc.boomerangm2=20}] run scoreboard players add @s tcc.boomerang 1
execute as @s[scores={tcc.boomerangm2=20}] run scoreboard players reset @s tcc.boomerangm2
