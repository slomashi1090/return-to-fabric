############################################################
# Description: Functions to tell which potion ring you're using
# Creator: CreeperMagnet_
############################################################
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:1s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/2
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:2s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/3
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:3s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/4
execute as @s[nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:unbreaking"}]}}]}] run scoreboard players set @s tcc.random 1

execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:night_vision"}}}]},nbt=!{ActiveEffects:[{Id:16b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:night_vision"}}}]},nbt=!{ActiveEffects:[{Id:16b}]}] run effect give @s night_vision 8 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_night_vision"}}}]},nbt=!{ActiveEffects:[{Id:16b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_night_vision"}}}]},nbt=!{ActiveEffects:[{Id:16b}]}] run effect give @s night_vision 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:water_breathing"}}}]},nbt=!{ActiveEffects:[{Id:13b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:water_breathing"}}}]},nbt=!{ActiveEffects:[{Id:13b}]}] run effect give @s water_breathing 8 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_water_breathing"}}}]},nbt=!{ActiveEffects:[{Id:13b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_water_breathing"}}}]},nbt=!{ActiveEffects:[{Id:13b}]}] run effect give @s water_breathing 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:fire_resistance"}}}]},nbt=!{ActiveEffects:[{Id:12b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:fire_resistance"}}}]},nbt=!{ActiveEffects:[{Id:12b}]}] run effect give @s fire_resistance 8 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_fire_resistance"}}}]},nbt=!{ActiveEffects:[{Id:12b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_fire_resistance"}}}]},nbt=!{ActiveEffects:[{Id:12b}]}] run effect give @s fire_resistance 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:invisibility"}}}]},nbt=!{ActiveEffects:[{Id:14b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:invisibility"}}}]},nbt=!{ActiveEffects:[{Id:14b}]}] run effect give @s invisibility 8 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_invisibility"}}}]},nbt=!{ActiveEffects:[{Id:14b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_invisibility"}}}]},nbt=!{ActiveEffects:[{Id:14b}]}] run effect give @s invisibility 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:leaping"}}}]},nbt=!{ActiveEffects:[{Id:8b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:leaping"}}}]},nbt=!{ActiveEffects:[{Id:8b}]}] run effect give @s jump_boost 8 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_leaping"}}}]},nbt=!{ActiveEffects:[{Id:8b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_leaping"}}}]},nbt=!{ActiveEffects:[{Id:8b}]}] run effect give @s jump_boost 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_leaping"}}}]},nbt=!{ActiveEffects:[{Id:8b,Amplifier:1b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_leaping"}}}]},nbt=!{ActiveEffects:[{Id:8b,Amplifier:1b}]}] run effect give @s jump_boost 4 1
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strength"}}}]},nbt=!{ActiveEffects:[{Id:5b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strength"}}}]},nbt=!{ActiveEffects:[{Id:5b}]}] run effect give @s strength 8 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_strength"}}}]},nbt=!{ActiveEffects:[{Id:5b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_strength"}}}]},nbt=!{ActiveEffects:[{Id:5b}]}] run effect give @s strength 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_strength"}}}]},nbt=!{ActiveEffects:[{Id:5b,Amplifier:1b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_strength"}}}]},nbt=!{ActiveEffects:[{Id:5b,Amplifier:1b}]}] run effect give @s strength 4 1
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:swiftness"}}}]},nbt=!{ActiveEffects:[{Id:1b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:swiftness"}}}]},nbt=!{ActiveEffects:[{Id:1b}]}] run effect give @s speed 8 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_swiftness"}}}]},nbt=!{ActiveEffects:[{Id:1b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_swiftness"}}}]},nbt=!{ActiveEffects:[{Id:1b}]}] run effect give @s speed 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_swiftness"}}}]},nbt=!{ActiveEffects:[{Id:1b,Amplifier:1b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_swiftness"}}}]},nbt=!{ActiveEffects:[{Id:1b,Amplifier:1b}]}] run effect give @s speed 4 1
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:slowness"}}}]},nbt=!{ActiveEffects:[{Id:2b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:slowness"}}}]},nbt=!{ActiveEffects:[{Id:2b}]}] run effect give @s slowness 5 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_slowness"}}}]},nbt=!{ActiveEffects:[{Id:2b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_slowness"}}}]},nbt=!{ActiveEffects:[{Id:2b}]}] run effect give @s slowness 10 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_slowness"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:3b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_slowness"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:3b}]}] run effect give @s slowness 2 3
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:turtle_master"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:3b},{Id:11b,Amplifier:2b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:turtle_master"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:3b},{Id:11b,Amplifier:2b}]}] run effect give @s resistance 5 2
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:turtle_master"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:3b},{Id:11b,Amplifier:2b}]}] run effect give @s slowness 5 3
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_turtle_master"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:3b},{Id:11b,Amplifier:2b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_turtle_master"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:3b},{Id:11b,Amplifier:2b}]}] run effect give @s resistance 10 2
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_turtle_master"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:3b},{Id:11b,Amplifier:2b}]}] run effect give @s slowness 10 3
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_turtle_master"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:5b},{Id:11b,Amplifier:3b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_turtle_master"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:5b},{Id:11b,Amplifier:3b}]}] run effect give @s resistance 2 3
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_turtle_master"}}}]},nbt=!{ActiveEffects:[{Id:2b,Amplifier:5b},{Id:11b,Amplifier:3b}]}] run effect give @s slowness 2 5


execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:regeneration"}}}]},nbt=!{ActiveEffects:[{Id:10b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:regeneration"}}}]},nbt=!{ActiveEffects:[{Id:10b}]}] run effect give @s regeneration 5 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_regeneration"}}}]},nbt=!{ActiveEffects:[{Id:10b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_regeneration"}}}]},nbt=!{ActiveEffects:[{Id:10b}]}] run effect give @s regeneration 10 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_regeneration"}}}]},nbt=!{ActiveEffects:[{Id:10b,Amplifier:1b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_regeneration"}}}]},nbt=!{ActiveEffects:[{Id:10b,Amplifier:1b}]}] run effect give @s regeneration 2 1
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:poison"}}}]},nbt=!{ActiveEffects:[{Id:19b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:poison"}}}]},nbt=!{ActiveEffects:[{Id:19b}]}] run effect give @s poison 5 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_poison"}}}]},nbt=!{ActiveEffects:[{Id:19b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_poison"}}}]},nbt=!{ActiveEffects:[{Id:19b}]}] run effect give @s poison 10 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_poison"}}}]},nbt=!{ActiveEffects:[{Id:19b,Amplifier:1b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_poison"}}}]},nbt=!{ActiveEffects:[{Id:19b,Amplifier:1b}]}] run effect give @s poison 2 1
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:weakness"}}}]},nbt=!{ActiveEffects:[{Id:18b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:weakness"}}}]},nbt=!{ActiveEffects:[{Id:18b}]}] run effect give @s weakness 2 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_weakness"}}}]},nbt=!{ActiveEffects:[{Id:18b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_weakness"}}}]},nbt=!{ActiveEffects:[{Id:18b}]}] run effect give @s weakness 5 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:slow_falling"}}}]},nbt=!{ActiveEffects:[{Id:28b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:slow_falling"}}}]},nbt=!{ActiveEffects:[{Id:28b}]}] run effect give @s slow_falling 2 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_slow_falling"}}}]},nbt=!{ActiveEffects:[{Id:28b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:long_slow_falling"}}}]},nbt=!{ActiveEffects:[{Id:28b}]}] run effect give @s slow_falling 5 0


execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:haste"}}}]},nbt=!{ActiveEffects:[{Id:3b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:haste"}}}]},nbt=!{ActiveEffects:[{Id:3b}]}] run effect give @s haste 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:mining_fatigue"}}}]},nbt=!{ActiveEffects:[{Id:4b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:mining_fatigue"}}}]},nbt=!{ActiveEffects:[{Id:4b}]}] run effect give @s mining_fatigue 10 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:nausea"}}}]},nbt=!{ActiveEffects:[{Id:9b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:nausea"}}}]},nbt=!{ActiveEffects:[{Id:9b}]}] run effect give @s nausea 5 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:resistance"}}}]},nbt=!{ActiveEffects:[{Id:11b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:resistance"}}}]},nbt=!{ActiveEffects:[{Id:11b}]}] run effect give @s resistance 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:blindness"}}}]},nbt=!{ActiveEffects:[{Id:15b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:blindness"}}}]},nbt=!{ActiveEffects:[{Id:15b}]}] run effect give @s blindness 5 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:hunger"}}}]},nbt=!{ActiveEffects:[{Id:17b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:hunger"}}}]},nbt=!{ActiveEffects:[{Id:17b}]}] run effect give @s hunger 5 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:wither"}}}]},nbt=!{ActiveEffects:[{Id:20b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:wither"}}}]},nbt=!{ActiveEffects:[{Id:20b}]}] run effect give @s wither 10 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:health_boost"}}}]},nbt=!{ActiveEffects:[{Id:21b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:health_boost"}}}]},nbt=!{ActiveEffects:[{Id:21b}]}] run effect give @s health_boost 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:absorption"}}}]},nbt=!{ActiveEffects:[{Id:22b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:absorption"}}}]},nbt=!{ActiveEffects:[{Id:22b}]}] run effect give @s absorption 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:glowing"}}}]},nbt=!{ActiveEffects:[{Id:24b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:glowing"}}}]},nbt=!{ActiveEffects:[{Id:24b}]}] run effect give @s glowing 10 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:levitation"}}}]},nbt=!{ActiveEffects:[{Id:25b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:levitation"}}}]},nbt=!{ActiveEffects:[{Id:25b}]}] run effect give @s levitation 5 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:luck"}}}]},nbt=!{ActiveEffects:[{Id:26b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:luck"}}}]},nbt=!{ActiveEffects:[{Id:26b}]}] run effect give @s luck 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:unluck"}}}]},nbt=!{ActiveEffects:[{Id:27b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:unluck"}}}]},nbt=!{ActiveEffects:[{Id:27b}]}] run effect give @s unluck 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:conduit_power"}}}]},nbt=!{ActiveEffects:[{Id:29b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:conduit_power"}}}]},nbt=!{ActiveEffects:[{Id:29b}]}] run effect give @s conduit_power 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:dolphins_grace"}}}]},nbt=!{ActiveEffects:[{Id:30b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:dolphins_grace"}}}]},nbt=!{ActiveEffects:[{Id:30b}]}] run effect give @s dolphins_grace 16 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:bad_omen"}}}]},nbt=!{ActiveEffects:[{Id:31b}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:bad_omen"}}}]},nbt=!{ActiveEffects:[{Id:31b}]}] run effect give @s bad_omen 30 0

execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:saturation"}}}]}] at @s[scores={tcc.hunger=..19}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:saturation"}}}]}] at @s[scores={tcc.hunger=..19}] run effect give @s saturation 1 0

execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:healing"}}}]}] at @s[scores={tcc.health=..19}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:healing"}}}]}] at @s[scores={tcc.health=..19}] run effect give @s instant_health 1 0

execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_healing"}}}]}] at @s[scores={tcc.health=..19}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_healing"}}}]}] at @s[scores={tcc.health=..19}] run effect give @s instant_health 1 1


execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:harming"}}}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:harming"}}}]}] run effect give @s[scores={tcc.health=7..}] instant_damage 1 0
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:harming"}}}]},scores={tcc.health=1..6}] run gamerule showDeathMessages false
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:harming"}}}]},scores={tcc.health=1..6}] run tellraw @a {"translate":"death.tcc.harming_ring","with":[{"selector":"@s"},{"nbt":"Inventory[{Slot:-106b}].tag.display.Name","entity":"@s","interpret":"true"}]}
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:harming"}}}]},scores={tcc.health=1..6}] run kill @s
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:harming"}}}]},scores={tcc.health=1..6}] run gamerule showDeathMessages true

execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_harming"}}}]}] run tag @s add tcc.tag
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_harming"}}}]}] run effect give @s[scores={tcc.health=13..}] instant_damage 1 1
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_harming"}}}]},scores={tcc.health=1..12}] run gamerule showDeathMessages false
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_harming"}}}]},scores={tcc.health=1..12}] run tellraw @a {"translate":"death.tcc.harming_ring","with":[{"selector":"@s"},{"nbt":"Inventory[{Slot:-106b}].tag.display.Name","entity":"@s","interpret":"true"}]}
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_harming"}}}]},scores={tcc.health=1..12}] run kill @s
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Potion:"minecraft:strong_harming"}}}]},scores={tcc.health=1..12}] run gamerule showDeathMessages true




execute as @s[tag=tcc.tag,scores={tcc.random=1},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]}] run loot replace entity @s weapon.offhand loot tcc:technical/durability/soaked_rings/iron_ring/offhand/break
execute as @s[tag=tcc.tag,scores={tcc.random=1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]},nbt={Inventory:[{Slot:-106b,id:"minecraft:carrot_on_a_stick"}]}] run function tcc:items/potion_items/soaked_rings/iron_ring/damage

tag @s remove tcc.tag
scoreboard players set @s tcc.random 0
