############################################################
# Description: Makes the cracked nether star work
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/6
execute as @s[scores={tcc.random=1}] unless entity @e[type=area_effect_cloud,distance=..3,nbt={Effects:[{Ambient:1b}]}] run summon area_effect_cloud ~ ~ ~ {Effects:[{Id:1,Duration:6000,Amplifier:0,Ambient:1b}],Radius:1.5,WaitTime:0,ReapplicationDelay:0,Duration:6,Tags:["tcc.area_effect_cloud.no_tip"]}
execute as @s[scores={tcc.random=2}] unless entity @e[type=area_effect_cloud,distance=..3,nbt={Effects:[{Ambient:1b}]}] run summon area_effect_cloud ~ ~ ~ {Effects:[{Id:3,Duration:6000,Amplifier:0,Ambient:1b}],Radius:1.5,WaitTime:0,ReapplicationDelay:0,Duration:6,Tags:["tcc.area_effect_cloud.no_tip"]}
execute as @s[scores={tcc.random=3}] unless entity @e[type=area_effect_cloud,distance=..3,nbt={Effects:[{Ambient:1b}]}] run summon area_effect_cloud ~ ~ ~ {Effects:[{Id:5,Duration:6000,Amplifier:0,Ambient:1b}],Radius:1.5,WaitTime:0,ReapplicationDelay:0,Duration:6,Tags:["tcc.area_effect_cloud.no_tip"]}
execute as @s[scores={tcc.random=4}] unless entity @e[type=area_effect_cloud,distance=..3,nbt={Effects:[{Ambient:1b}]}] run summon area_effect_cloud ~ ~ ~ {Effects:[{Id:8,Duration:6000,Amplifier:0,Ambient:1b}],Radius:1.5,WaitTime:0,ReapplicationDelay:0,Duration:6,Tags:["tcc.area_effect_cloud.no_tip"]}
execute as @s[scores={tcc.random=5}] unless entity @e[type=area_effect_cloud,distance=..3,nbt={Effects:[{Ambient:1b}]}] run summon area_effect_cloud ~ ~ ~ {Effects:[{Id:10,Duration:6000,Amplifier:0,Ambient:1b}],Radius:1.5,WaitTime:0,ReapplicationDelay:0,Duration:6,Tags:["tcc.area_effect_cloud.no_tip"]}
execute as @s[scores={tcc.random=6}] unless entity @e[type=area_effect_cloud,distance=..3,nbt={Effects:[{Ambient:1b}]}] run summon area_effect_cloud ~ ~ ~ {Effects:[{Id:11,Duration:6000,Amplifier:0,Ambient:1b}],Radius:1.5,WaitTime:0,ReapplicationDelay:0,Duration:6,Tags:["tcc.area_effect_cloud.no_tip"]}
scoreboard players reset @s tcc.random

advancement grant @s[advancements={tcc:minecraft/nether/cracked_nether_star=false}] only tcc:minecraft/nether/cracked_nether_star
