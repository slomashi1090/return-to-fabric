############################################################
# Description: Closes your elytra when shifting
# Creator: CreeperMagnet_
############################################################

scoreboard players reset @s tcc.useelytra
scoreboard players reset @s tcc.shifttime
loot replace entity @s armor.chest loot tcc:technical/elytra_close/close
tag @s add tcc.schedule.elytra_close
schedule function tcc:items/elytra_close/scheduled 2t
