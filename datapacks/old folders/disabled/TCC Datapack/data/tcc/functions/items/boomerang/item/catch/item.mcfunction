############################################################
# Description: Spawns a boomerang item with accurate nbt and damage
# Creator: CreeperMagnet_
############################################################

function tcc:items/boomerang/item/catch/damage
summon item ~ ~ ~ {Item:{id:"minecraft:carrot_on_a_stick",Count:1b,tag:{CustomModelData:1}},Tags:["tcc.boomerang.item"]}
data modify entity @e[type=item,limit=1,tag=tcc.boomerang.item] Item.tag set from entity @s ArmorItems[3].tag
tag @e[type=item,limit=1,tag=tcc.boomerang.item] remove tcc.boomerang.item