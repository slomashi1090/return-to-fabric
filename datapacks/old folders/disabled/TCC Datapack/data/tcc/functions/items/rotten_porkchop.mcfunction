############################################################
# Description: Makes rotten porkchops give you nausea
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/items/rotten_porkchop
effect give @s nausea 30 0
execute as @e[type=zombie_pigman,distance=..20,sort=arbitrary] run playsound minecraft:entity.zombie_pig.angry hostile @a[distance=..100] ~ ~ ~ 100 1
execute as @e[type=zombie_pigman,distance=..20,sort=arbitrary] run data merge entity @s {Anger:400}