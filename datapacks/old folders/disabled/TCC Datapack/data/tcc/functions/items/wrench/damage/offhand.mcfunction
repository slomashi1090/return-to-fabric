############################################################
# Description: Makes the wrench damage function
# Creator: CreeperMagnet_
############################################################
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:1s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/2
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:2s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/3
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:3s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/4
execute as @s[nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:unbreaking"}]}}]}] run scoreboard players set @s tcc.random 1

execute as @s[scores={tcc.random=1},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]}] run playsound minecraft:entity.item.break player @a[distance=..16]
execute as @s[scores={tcc.random=1},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]}] run particle item minecraft:carrot_on_a_stick{CustomModelData:8} ~ ~1 ~ 0.2 0.2 0.2 0.1 10 force
execute as @s[scores={tcc.random=1},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]}] run replaceitem entity @s weapon.offhand air

execute as @s[scores={tcc.random=1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"wrench"}}}]}] store result score @s tcc.math run data get entity @s Inventory[{Slot:-106b}].tag.tcc.Durability 1
execute as @s[scores={tcc.random=1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"wrench"}}}]}] run scoreboard players remove @s tcc.math 1
execute as @s[scores={tcc.random=1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"wrench"}}}]}] store result entity @s Inventory[{Slot:-106b}].tag.tcc.Durability int 1 run scoreboard players get @s tcc.math
execute as @s[scores={tcc.random=1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"wrench"}}}]}] run scoreboard players reset @s tcc.math

execute as @s[scores={tcc.random=1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"wrench"}}}]}] run loot replace entity @s weapon.offhand loot tcc:technical/durability/wrench/offhand/store
execute as @s[scores={tcc.random=1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"wrench"}}}]}] run loot replace entity @s weapon.offhand loot tcc:technical/durability/wrench/offhand/reorder

scoreboard players reset @s tcc.random
