############################################################
# Description: Damages potion-soaked iron rings
# Creator: CreeperMagnet_
############################################################

execute store result score @s tcc.math run data get entity @s Inventory[{Slot:-106b}].tag.tcc.Durability 1
scoreboard players add @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:49}}}]},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:50}}}]}] tcc.math 2
scoreboard players add @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Durability:49}}}]},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:50}}}]}] tcc.math 1
execute store result entity @s Inventory[{Slot:-106b}].tag.tcc.Durability int 1 run scoreboard players get @s tcc.math
scoreboard players reset @s tcc.math
