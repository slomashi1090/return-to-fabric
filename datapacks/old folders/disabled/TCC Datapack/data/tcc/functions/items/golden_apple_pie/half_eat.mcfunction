############################################################
# Description: Makes golden apple pie halves work
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/items/golden_apple_pie_half
tag @s[nbt={SelectedItem:{tag:{tcc:{Item:"golden_apple_pie_half"}}}}] add tcc.scheduled.golden_apple_pie_half.mainhand
tag @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"golden_apple_pie_half"}}}]}] add tcc.scheduled.golden_apple_pie_half.offhand
schedule function tcc:items/golden_apple_pie/slice_insert 1t
effect give @s regeneration 10 1
effect give @s absorption 240 0
effect give @s saturation 1 4 true
