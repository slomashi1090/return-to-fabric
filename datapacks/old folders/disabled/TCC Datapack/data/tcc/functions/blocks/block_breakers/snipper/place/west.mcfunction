############################################################
# Description: Commands for placing a block breaker
# Creator: CreeperMagnet_
############################################################

setblock ~ ~ ~ dropper[facing=east]{CustomName:"{\"translate\":\"block.tcc.snipper\"}"}
summon armor_stand ~ ~0.5 ~ {Rotation:[90.0f,0.0f],ArmorItems:[{},{},{},{id:"minecraft:barrier",Count:1b,tag:{CustomModelData:1}}],Tags:["tcc.blocks.snipper","tcc.block_breakers.west","tcc.blocks.block_breaker"],NoGravity:1b,Invisible:1b,Marker:1b}
