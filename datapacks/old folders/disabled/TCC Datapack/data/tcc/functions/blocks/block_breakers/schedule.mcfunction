############################################################
# Description: Schedules and sets tags for block breaking 5 ticks later in time.
# Creator: CreeperMagnet_
############################################################


execute as @s[tag=tcc.blocks.excavator,tag=!tcc.scheduled.block_breaker] if block ~ ~ ~ #tcc:excavator_breakable run schedule function tcc:blocks/block_breakers/break_block 5t
execute as @s[tag=tcc.blocks.excavator,tag=!tcc.scheduled.block_breaker] if block ~ ~ ~ #tcc:excavator_breakable run tag @s add tcc.scheduled.block_breaker

execute as @s[tag=tcc.blocks.chopper,tag=!tcc.scheduled.block_breaker] if block ~ ~ ~ #tcc:chopper_breakable run schedule function tcc:blocks/block_breakers/break_block 5t
execute as @s[tag=tcc.blocks.chopper,tag=!tcc.scheduled.block_breaker] if block ~ ~ ~ #tcc:chopper_breakable run tag @s add tcc.scheduled.block_breaker

execute as @s[tag=tcc.blocks.sifter,tag=!tcc.scheduled.block_breaker] if block ~ ~ ~ #tcc:sifter_breakable run schedule function tcc:blocks/block_breakers/break_block 5t
execute as @s[tag=tcc.blocks.sifter,tag=!tcc.scheduled.block_breaker] if block ~ ~ ~ #tcc:sifter_breakable run tag @s add tcc.scheduled.block_breaker

execute as @s[tag=tcc.blocks.snipper,tag=!tcc.scheduled.block_breaker] if block ~ ~ ~ #tcc:snipper_breakable run schedule function tcc:blocks/block_breakers/break_block 5t
execute as @s[tag=tcc.blocks.snipper,tag=!tcc.scheduled.block_breaker] if block ~ ~ ~ #tcc:snipper_breakable run tag @s add tcc.scheduled.block_breaker
