############################################################
# Description: Tells what type of block breaker is powered
# Creator: CreeperMagnet_
############################################################


execute as @s[tag=tcc.block_breakers.down,tag=!tcc.scheduled.block_breaker] positioned ~ ~-2 ~ run function tcc:blocks/block_breakers/schedule
execute as @s[tag=tcc.block_breakers.up,tag=!tcc.scheduled.block_breaker] positioned ~ ~ ~ run function tcc:blocks/block_breakers/schedule
execute as @s[tag=tcc.block_breakers.north,tag=!tcc.scheduled.block_breaker] positioned ~ ~-1 ~-1 run function tcc:blocks/block_breakers/schedule
execute as @s[tag=tcc.block_breakers.south,tag=!tcc.scheduled.block_breaker] positioned ~ ~-1 ~1 run function tcc:blocks/block_breakers/schedule
execute as @s[tag=tcc.block_breakers.east,tag=!tcc.scheduled.block_breaker] positioned ~1 ~-1 ~ run function tcc:blocks/block_breakers/schedule
execute as @s[tag=tcc.block_breakers.west,tag=!tcc.scheduled.block_breaker] positioned ~-1 ~-1 ~ run function tcc:blocks/block_breakers/schedule

tag @s add tcc.block_breakers.powered
