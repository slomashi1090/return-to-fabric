############################################################
# Description: Commands for breaking block breakers
# Creator: CreeperMagnet_
############################################################

execute as @s[tag=tcc.blocks.excavator] run function tcc:blocks/block_breakers/excavator/break
execute as @s[tag=tcc.blocks.sifter] run function tcc:blocks/block_breakers/sifter/break
execute as @s[tag=tcc.blocks.chopper] run function tcc:blocks/block_breakers/chopper/break
execute as @s[tag=tcc.blocks.snipper] run function tcc:blocks/block_breakers/snipper/break
