############################################################
# Description: Commands for placing a block breaker
# Creator: CreeperMagnet_
############################################################

setblock ~ ~ ~ dropper[facing=down]{CustomName:"{\"translate\":\"block.tcc.excavator\"}"}
summon armor_stand ~ ~0.5 ~ {Rotation:[180.0f,0.0f],ArmorItems:[{},{},{},{id:"minecraft:barrier",Count:1b,tag:{CustomModelData:3}}],Tags:["tcc.blocks.excavator","tcc.block_breakers.up","tcc.blocks.block_breaker"],NoGravity:1b,Invisible:1b,Marker:1b}
